<footer class="text-muted bg-light fixed-bottom">
  <div class="container">
    <p class="float-right m-0">
      <a href="#"><i class="fas fa-arrow-up"></i></a>
    </p>
    <p class="m-0">The Remote Rescue Plan is trademarked by <a href src="#">ACE Air & Ambulance (Pvt) Ltd</a></p>
    <!-- <p>New to this Remote Rescue Subscription tool? <a href="../../">Visit the User tutorial page</a>.</p> -->
  </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

@yield('scripts')