<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="{{ route('individual') }}">Remote Rescue</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      @if(Auth::check())
      <ul class="navbar-nav mr-auto">
          @if(Auth::user()->role_id < 3)
        <li class="nav-item dropdown {{ Request::is('subscribers/create', 'groups/create') ? 'active' : '' }}">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Add
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('subscribers.create') }}">Subscriber</a>
            <a class="dropdown-item" href="{{ route('family.create') }}">Family</a>
            <a class="dropdown-item" href="{{ route('corporate.create') }}">Corporate</a>
            <a class="dropdown-item" href="{{ route('safari.create') }}">Safari Camp</a>
          </div>
        </li>
        @endif
        <li class="nav-item {{ Request::is('individual', 'family', 'corporate', 'safari', 'region', 'regional') ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('individual') }}">Dashboards</a>
        </li>
      </ul>

      <form action="{{ url('/search') }}" method="POST" class="input-group search-bar-group my-2 my-lg-0">
          {{ csrf_field() }}
        <input class="form-control search-bar" name="search" placeholder="Search Subscribers by name">
        <div class="input-group-append">
          <button class="btn btn-search-bar" type="submit" id="button-addon2"><i class="fas fa-search"></i></button>
        </div>
      </form>

      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->first }}
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('administrators.show', Auth::user()->id) }}">Profile</a>
            <a class="dropdown-item" href="{{ route('admin.logout') }}">Logout</a>
          </div>
        </li>

        <li class="nav-item dropdown {{ Request::is('questions', 'packages', 'group_types') ? 'active' : '' }}">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-cog"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('questions.index') }}">Questions</a>
            <a class="dropdown-item" href="{{ route('packages.index') }}">Packages</a>
            {{-- <a class="dropdown-item" href="{{ route('group_types.index') }}">Group Types</a> --}}

            @if(Auth::user()->role_id == 1)
            <a class="dropdown-item bg-danger text-light" href="{{ route('administrators.index') }}">Administrators</a>
            @endif
            
          </div>
        </li>        
      </ul>
      @endif
    </div>
  </div>
</nav>