
<div class="list-group list-group-flush">
	<a href="{{ route('individual') }}" class="list-group-item list-group-item-action {{ Request::is('individual') ? 'active' : '' }}">Individuals</a>
	{{-- <a href="{{ route('group') }}" class="list-group-item list-group-item-action ">Groups</a> --}}
  <a href="{{ route('family.index') }}" class="list-group-item list-group-item-action {{ Request::is('family') ? 'active' : '' }}">Family</a>
  <a href="{{ route('corporate.index') }}" class="list-group-item list-group-item-action {{ Request::is('corporate') ? 'active' : '' }}">Corporate</a>
  <a href="{{ route('safari.index') }}" class="list-group-item list-group-item-action {{ Request::is('safari') ? 'active' : '' }}">Safari Camps</a>
  <a href="{{ route('region') }}" class="list-group-item list-group-item-action {{ Request::is('region') ? 'active' : '' }}">Regional</a>
	<a href="{{ route('regional') }}" class="list-group-item list-group-item-action {{ Request::is('regional') ? 'active' : '' }}">Regional+</a>
	<a href="{{ route('daily') }}" class="list-group-item list-group-item-action {{ Request::is('daily') ? 'active' : '' }}">Daily</a>
</div>

{{-- <div class="accordion side-menu pb-5" id="accordionExample">

	<div class="card">
	  <div class="card-header {{ Request::is('individual') ? 'active' : '' }}" id="headingOne">
		  <a href="{{ route('individual') }}" class="btn btn-link p-0 {{ Request::is('individual') ? 'active' : '' }}">Individuals</a>
	  </div>
  </div>
  
	<div class="card">
	  <div class="card-header d-flex justify-content-between align-items-center {{ Request::is('family', 'corporate', 'safari') ? 'active' : '' }}" id="headingTwo">
      <button class="btn btn-link p-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Groups
      </button>
      <i class="fas fa-caret-down" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"></i>
	  </div>
	  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body p-0">
        <div class="list-group list-group-flush side-menu-drop">
          <a href="{{ route('family') }}" class="list-group-item list-group-item-action {{ Request::is('family') ? 'active' : '' }}">Family</a>
          <a href="{{ route('corporate') }}" class="list-group-item list-group-item-action {{ Request::is('corporate') ? 'active' : '' }}">Corporate</a>
          <a href="{{ route('safari') }}" class="list-group-item list-group-item-action {{ Request::is('safari') ? 'active' : '' }}">Safari Camps</a>
        </div>
      </div>
	  </div>
  </div>
  
	<div class="card">
    <div class="card-header {{ Request::is('region') ? 'active' : '' }}" id="headingThree">
      <a href="{{ route('region') }}" class="btn btn-link p-0 {{ Request::is('region') ? 'active' : '' }}">Regional</a>
    </div>
  </div>

  <div class="card">
    <div class="card-header {{ Request::is('regional') ? 'active' : '' }}" id="headingFour">
      <a href="{{ route('regional') }}" class="btn btn-link p-0 {{ Request::is('regional') ? 'active' : '' }}">Regional Ex</a>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header {{ Request::is('daily') ? 'active' : '' }}" id="headingFive">
      <a href="{{ route('daily') }}" class="btn btn-link p-0 {{ Request::is('daily') ? 'active' : '' }}">Daily</a>
    </div>
  </div>
</div> --}}
