<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Admin Portal</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
      html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
      }

      nav {
        height: 80px;
      }

      footer {
        height: 30px;
      }

      .col-md-12 {
        height: calc(100vh - 110px);
      }

      h1 {
        font-size: 4rem;
      }

      footer {
        font-size: .8rem;
      }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container"> 
        <ul class="navbar-nav ml-auto">
          @if (Route::has('login'))
          <li class="nav-item">
            @if (Auth::check())
              <a class="nav-link" href="{{ route('individual') }}">Home</a>
            @else
              <a class="nav-link" href="{{ url('admin/login') }}">Admin Login</a>
              {{-- <a href="{{ url('/register') }}">Register</a> --}}
            @endif
          </li>
          @endif
        </ul>
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex flex-column justify-content-center align-items-center">
          <h1>Remote Rescue</h1>
          <p class="lead">Subscription Manager</p>
        </div>
      </div>
    </div>

    <footer class="bg-light">
      <div class="container">
        <p class="text-center mb-0 pt-2">Remote Rescue is a product of ACE Air &amp; Ambulance (Pvt) Ltd</p>
      </div>
    </footer

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>

