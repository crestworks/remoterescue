@extends('main')

@section('title', '| Create Safari')

@section('stylesheets')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')

<div class="row pb-80">
  <div class="col-md-6 offset-md-3">
    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Create Safari</h3>
      <a href="{{ route('safari.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
    </div>
    <hr>
    <form action="{{ url('safari') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="name">Camp Name</label>
        <input type="text" class="form-control" name="name" placeholder="Company Name" value="{{ old('name') }}">
      </div>
      <div class="form-group">
        <label for="beds">Beds</label>
        <input type="number" class="form-control" name="beds" placeholder="12" value="{{ old('beds') }}">
      </div>
      <div class="form-group">
        <label for="coordinates">Coordinates</label>
        <input type="text" class="form-control" name="coordinates" placeholder="17°47'58.8&quot;S 31°00'30.4&quot;E" value="{{ old('coordinates') }}">
      </div>
      <div class="form-group">
        <label for="location_description">Loaction Description</label>
        <textarea type="text" class="form-control" name="location_description" placeholder="How to get there...">{{ old('location_description') }}</textarea>
        <small id="addressHelp" class="form-text text-muted">
          A description of where the Camp is located.
        </small>
      </div>
      <div class="form-group">
        <label for="emergency_contact">Emergency Contact</label>
        <select class="form-control" name="emergency_contact">
          <option>Select Contact</option>
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="emergencyContactHelp" class="form-text text-muted">
          The above individual is the go-to person in case of emergencies.
        </small>
      </div>
      <div class="form-group">
        <label for="emergency_phone">Emergency Contact Number(s)</label>
        <input type="text" class="form-control" name="emergency_phone" placeholder="List all emergency contact number(s)..." value="{{ old('emergency_phone') }}">
      </div>
      <div class="form-group">
        <label for="account_rep">Account Representative</label>
        <select class="form-control" name="account_rep">
          <option>Select Representative</option>
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="accountRepHelp" class="form-text text-muted">
          The above individual is the go-to person for account queries.
        </small>
      </div>
      <div class="form-group">
        <label for="account_rep_phone">Account Representative Contact Number(s)</label>
        <input type="text" class="form-control" name="account_rep_phone" placeholder="List all account rep contact number(s)..." value="{{ old('account_rep_phone') }}">
      </div>
      <div class="form-group">
        <label for="account_rep_email">Account Representative Email</label>
        <input type="email" class="form-control" name="account_rep_email" placeholder="account@rep.com" value="{{ old('account_rep_email') }}">
      </div>
      <div class="form-group">
        <label for="website">Company Website</label>
        <input type="text" class="form-control" name="website" placeholder="www.website.com" value="{{ old('website') }}">
      </div>
      <div class="form-group">
        <label for="nearest_ace">Nearest ACE Base</label>
        <input type="text" class="form-control" name="nearest_ace" placeholder="Victoria Falls Base" value="{{ old('nearest_ace') }}">
      </div>
      <div class="form-group">
        <label for="nearest_airstrip">Nearest Airport/Airstrip</label>
        <input type="text" class="form-control" name="nearest_airstrip" placeholder="Victoria Falls (FVFA)" value="{{ old('nearest_airstrip') }}">
        <small id="nearestAirstripHelp" class="form-text text-muted">
          Please include the name of the airstrip and identifier (for flight planning).
        </small>
      </div>
      <div class="form-group">
        <label for="subscribers">Dependants</label>
        <select class="form-control select2-multi" multiple="multiple" name="subscribers[]" placeholder="Add dependants">
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="subscriberHelp" class="form-text text-muted">
          Before you add dependants they have to be in the system already. 
          If they are not please add them <a href="{{ route('subscribers.create') }}">here.</a></small>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
      </div>
    </form>
  </div>
</div>

@stop

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}" rel="stylesheet"></script>

  <script type="text/javascript">
    $('.select2-multi').select2();
    $('.select2-multi').select2().val({!! json_encode($subscribers) !!}).trigger('change');
  </script>
@stop