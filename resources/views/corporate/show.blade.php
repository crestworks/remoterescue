@extends('main')

@section('title', '| Show Corporate')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <div class="title-section d-flex justify-content-between align-items-baseline">
      <h2>Corporate- {{ $corporate->name }}</h2>
      <div class="title-section-btn-group">
        @if(Auth::user()->role_id < 3)
        <a href="{{ route('corporate.edit', $corporate->id) }}"><i class="fas fa-pencil-alt"></i></a>
        @endif
        <a href="{{ route('corporate.index') }}" class="ml-2"><i class="fas fa-times"></i></a>
      </div>
    </div>
    <h4>{{ Helper::rr_group_id( $group_id) }}</h4>
    <div>
      <div class="form-group">
        <label for="name">Name</label>
        <input readonly type="text" class="form-control" name="name" value="{{ $corporate->name }}">
      </div>
      <div class="form-group">
        <label for="address">Address</label>
        <input readonly type="text" class="form-control" name="address" value="{{ $corporate->address }}">
      </div>
      <div class="form-group">
        <label for="emergency_contact">Emergency Contact</label>
        <input readonly type="text" class="form-control" name="emergency_contact" value="{{ $emergency_contact->first }} {{ $emergency_contact->last }}">
      </div>
      <div class="form-group">
        <label for="emergency_phone">Emergency Contact Phone</label>
        <input readonly type="text" class="form-control" name="emergency_phone" value="{{ $corporate->emergency_phone }}">
      </div>
      <div class="form-group">
        <label for="account_rep">Account Representative</label>
        <input readonly type="text" class="form-control" name="account_rep" value="{{ $account_rep->first }} {{ $account_rep->last }}">
      </div>
      <div class="form-group">
        <label for="account_rep_phone">Account Representative Phone</label>
        <input readonly type="text" class="form-control" name="account_rep_phone" value="{{ $corporate->account_rep_phone }}">
      </div>
      <div class="form-group">
        <label for="account_rep_email">Account Representative Email</label>
        <input readonly type="text" class="form-control" name="account_rep_email" value="{{ $corporate->account_rep_email }}">
      </div>
      <div class="form-group">
        <label for="website">Website</label>
        <input readonly type="text" class="form-control" name="website" value="{{ $corporate->website }}">
      </div>
    </div>


    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Dependants</h3>
      <h5><i class="fas fa-users"></i> {{ count($corporate->subscribers) }}</h5>
    </div>
    @if(count($corporate->subscribers) > 0)
    {{-- <small>If the number of dependants is greater than that expicitly indicated (above), the extra dependants will be highlighted.</small> --}}
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Account#</th>
            <th scope="col">Primary</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($corporate->subscribers as $subscriber)
          <tr>
            <td>{{ $subscriber->first }} {{ $subscriber->last }}</td>
            <td>{{ Helper::rr_subscriber_id( $subscriber->id) }}</td>
            <td>
              <span class="badge badge-pill badge-primary mr-2" {{ ($subscriber->id == $corporate->emergency_contact) ? '' : 'hidden' }}>E</span>
              <span class="badge badge-pill badge-primary mr-2" {{ ($subscriber->id == $corporate->account_rep) ? '' : 'hidden' }}>R</span>
            </td>
            <td>
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('subscribers.show', $subscriber->id) }}" class="mr-2"><i class="far fa-eye"></i></a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    @else
    <div class="card bg-light mb-3">
      <div class="card-body">
        There are currently no dependants for this Group.
      </div>
    </div>
    @endif

    <div>
      <div class="d-flex justify-content-between align-items-baseline">
        <h3>Payments</h3>
        <div>
          <a href="#" class="ml-2"></a>
          <a class="card-link ml-2"
            href="#"
            data-toggle="modal"
            data-target="#createPaymentModal"
            data-id="{{ $corporate->id }}"
            data-url="{{ url('payments') }}">
              <i class="fas fa-plus"></i>
          </a>
          <a href="{{ url('payment', $corporate->id) }}" class="ml-2"><i class="fas fa-file-invoice-dollar"></i></a>
        </div>
      </div>

      @if(isset($payment))
        @if((new DateTime(date('Y-m-d', strtotime($payment->start_date))))->diff(new DateTime(date('Y-m-d', strtotime($payment->end_date))))->format('%m') == 1)
        {{-- @if((round(strtotime($payment->end_date) - strtotime($payment->start_date)) / (60 * 60 * 24)) == 31) --}}
        <div class="card text-white bg-dark mb-3">
        {{-- @elseif((new DateTime(date('Y-m-d', strtotime($payment->end_date))))->diff(new DateTime(date('Y-m-d', strtotime(time()))))->format('%d') > 30) --}}
        @elseif((round(strtotime($payment->end_date) - time()) / (60 * 60 * 24)) > 30)
        <div class="card text-white bg-primary mb-3">
        {{-- @elseif((new DateTime(date('Y-m-d', strtotime($payment->end_date))))->diff(new DateTime(date('Y-m-d', strtotime(time()))))->format('%d') < 0) --}}
        @elseif((round(strtotime($payment->end_date) - time()) / (60 * 60 * 24)) < 0)
        <div class="card text-white bg-danger mb-3">
        @elseif((new DateTime(date('Y-m-d', strtotime($payment->end_date))))->diff(new DateTime(date('Y-m-d', strtotime(time()))))->format('%d') < 31)
        {{-- @elseif((round(strtotime($payment->end_date) - time())  / (60 * 60 * 24)) < 31) --}}
        <div class="card bg-warning mb-3">
        @else
        <div class="card bg-light mb-3">
        @endif
          <div class="card-body">
            <div class="payment-card-header d-flex justify-content-between align-items-baseline">
              <h5 class="card-title m-0">${{ $payment->amount }}</h5>
              <h6 class="card-subtitle m-0">{{ date('M j, Y', strtotime($payment->paid_date)) }}</h6>
            </div>
            <hr>
            <p class="card-text">Payment Period: {{ date('M j, Y', strtotime($payment->start_date)) }} to {{ date('M j, Y', strtotime($payment->end_date)) }}</p>
            <p class="card-text">Days Remaining: {{ round(round(strtotime($payment->end_date) - time()) / (60 * 60 * 24)) }} days</p>
            {{-- <p class="card-text">Total Balance: -$21 (debit)</p> --}}
            @if((round(strtotime($payment->end_date) - strtotime($payment->start_date)) / (60 * 60 * 24)) == 31)
            <div class="card card-note-dark">
              <div class="card-body pt-1 pb-1">
                <small calss="text-muted">
                  Notes: The payment was incorrect<br>
                  - this could be an error on the part of the person entering the data<br>
                  - or the primary must be contacted<br>
                </small>
              </div>
            </div>
            @elseif((round(strtotime($payment->end_date) - time()) / (60 * 60 * 24)) > 30)
            <div class="card card-note-primary">
              <div class="card-body pt-1 pb-1">
                <small calss="text-muted">
                  Notes: Payments are up-to-date and accurate
                </small>
              </div>
            </div>
            @elseif((round(strtotime($payment->end_date) - time()) / (60 * 60 * 24)) < 0)
            <div class="card card-note-danger">
              <div class="card-body pt-1 pb-1">
                <small calss="text-muted">
                  Notes: Payment period has expired<br>
                  - all dependants are no longer covered<br>
                  - please note this could also mean the rectification window has expired
                </small>
              </div>
            </div>
            @elseif((round(strtotime($payment->end_date) - time())  / (60 * 60 * 24)) < 31)
            <div class="card card-note-warning">
              <div class="card-body pt-1 pb-1">
                <small calss="text-muted">
                  Notes: The payment period is about to expire<br>
                  - please note this could also mean the rectification window is about to expired
                </small>
              </div>
            </div>
            @else
            <div class="card">
              <div class="card-body pt-1 pb-1">
                  <small calss="text-muted">
                    Notes: Something has gone wrong and this must be reported to the developer.
                  </small>
                </div>
              </div>
            @endif
          </div>
        </div>
      @else
      <div class="card bg-light mb-3">
        <div class="card-body">
          Currently no payments have been entered!
        </div>
      </div>
      @endif
    </div>

    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Comments</h3>
      <h5><i class="fas fa-comments"></i> {{ count($comments) }}</h5>
    </div>
    
    <div class="card bg-light mb-3">
      <div class="card-body pt-0 pb-0">

        <form class="mt-3 mb-3" action="{{ url('comment', $corporate->id) }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <textarea rows="3" class="form-control" name="comment" placeholder="Write your comment here..."></textarea>
          </div>
          <button type="submit" class="btn btn-block btn-primary">Comment</button> 
        </form>   
        @foreach($comments as $comment)
        <div class="comment mt-3 mb-3">
          <p class="card-text mt-1">{{ $comment->comment }}
              @if($comment->admin_id == Auth::user()->id)
              &nbsp;<a class="card-link"
                    href="#"
                    data-toggle="modal"
                    data-target="#editCommentModal"
                    data-id="{{ $comment->id }}"
                    data-url="{{ url('comment') }}"
                    data-comment="{{ $comment->comment }}"><i class="fas fa-pencil-alt"></i></a>
              @endif
            </p>
          <div class="d-flex justify-content-between align-items-baseline">
            <small>- {{ $comment->admin->first }} {{ $comment->admin->last }}</small>
            <small>Updated: {{ date('M j, Y H:i', strtotime($comment->updated_at)) }}</small>
          </div>
          {{-- <button class="btn btn-xs btn-secondary" data-toggle="modal" data-target="#modal{{ $comment->id }}">Edit Comment</button> --}}
        </div>
        @endforeach
      </div>
    </div>

    <!-- Edit Comment Modal -->
    <div class="modal fade" id="editCommentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          
    
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit-comment-form" action="{{ url('comment', '1') }}" method="POST">
  
              {{ method_field('PUT') }}
              {{ csrf_field() }}

              <input id="comment-id" type="text" name="id" value="0" hidden>
              <textarea name="comment" class="form-control" placeholder="Write your comment here..."></textarea>
              <div class="mt-3 d-flex justify-content-between">
                <button class="btn btn-outline-danger" type="button" data-toggle="collapse" data-target="#collapseDelete" aria-expanded="false" aria-controls="collapseExample">
                  Delete
                </button>
                <div>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
              </div>
            </form>

            <form class="alert alert-danger collapse mt-3 mb-0" id="collapseDelete" action="{{ url('comment', '1') }}" method="POST">
  
              {{ method_field('DELETE') }}
              {{ csrf_field() }}
      
              <p>You are about to delete this Comment and all associated data. Are you sure you want to go ahead with this?</p>
              <input type="text" id="comment-delete-input" name="id" hidden>
              <button type="submit" class="btn btn-danger btn-block">Confirm</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Create Payment Modal -->
    <div class="modal fade" id="createPaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          
    
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Payment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="create-payment-form" action="{{ url('payment', $corporate->id) }}" method="POST">
  
              {{ csrf_field() }}

              <div class="form-group">
                <label for="amount">Payment Amount 
                  <a 
                    tabindex="0" 
                    class="btn" 
                    role="button" 
                    data-toggle="popover" 
                    data-trigger="focus" 
                    title="Package Payments" 
                    data-content=
                      "And here's some amazing content. It's very engaging. Right?">
                      <i class="fas fa-info-circle"></i>
                  </a>
                </label>
                <input type="text" class="form-control" name="amount" placeholder="99.99" value="{{ old('amount') }}">
              </div>
              <div class="form-group">
                <label for="paid_date">Date Paid</label>
                <input type="date" class="form-control" name="paid_date" placeholder="MM/DD/YYYY" value="{{ old('paid_date') }}">
              </div>
              {{-- <div class="form-group">
                <label for="duration">Payment Duration</label>
                <input type="number" class="form-control" name="duration" placeholder="6" value="{{ old('duration') }}">
              </div> --}}
              <div class="mt-3 d-flex justify-content-end">
                {{-- <button type="button" class="btn btn-secondary ml-2" data-dismiss="modal">Close</button> --}}
                <button type="submit" class="btn btn-primary btn-block">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">

  $('#editCommentModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var actionURL = button.data('url')
    var commentId = button.data('id')
    var commentText = button.data('comment') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('#comment-id').val(commentId)
    modal.find('#comment-delete-input').val(commentId)
    modal.find('.modal-body textarea').val(commentText)
    modal.find('#edit-comment-form').attr('action', actionURL + '/' + commentId);
    modal.find('#collapseDelete').attr('action', actionURL + '/' + commentId);
    // modal.find('#edit-comment-form').action = actionURL + '/' + commentId
    // alert(actionURL + '/' + commentId)
  });

</script>
@stop
