@extends('main')

@section('title', '| Create Corporation')

@section('stylesheets')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')

<div class="row pb-80">
  <div class="col-md-6 offset-md-3">
    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Create Corporation</h3>
      <a href="{{ route('corporate.index') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
    </div>
    <hr>
    <form action="{{ url('corporate') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="name">Company Name</label>
        <input type="text" class="form-control" name="name" placeholder="Company Name" value="{{ old('name') }}">
      </div>
      <div class="form-group">
        <label for="address">Company Address</label>
        <input type="text" class="form-control" name="address" placeholder="Physical address" value="{{ old('address') }}">
        <small id="addressHelp" class="form-text text-muted">
          This must be a physical address and not a PO Box address. 
          We need to know where to respond to in case of emergencies.
        </small>
      </div>
      <div class="form-group">
        <label for="emergency_contact">Emergency Contact</label>
        <select class="form-control" name="emergency_contact">
          <option>Select Contact</option>
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="emergencyContactHelp" class="form-text text-muted">
          The above individual is the go-to person in case of emergencies.
        </small>
      </div>
      <div class="form-group">
        <label for="emergency_phone">Emergency Contact Number(s)</label>
        <input type="text" class="form-control" name="emergency_phone" placeholder="List all emergency contact number(s)..." value="{{ old('emergency_phone') }}">
      </div>
      <div class="form-group">
        <label for="account_rep">Account Representative</label>
        <select class="form-control" name="account_rep">
          <option>Select Representative</option>
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="accountRepHelp" class="form-text text-muted">
          The above individual is the go-to person for account queries.
        </small>
      </div>
      <div class="form-group">
        <label for="account_rep_phone">Account Representative Contact Number(s)</label>
        <input type="text" class="form-control" name="account_rep_phone" placeholder="List all account rep contact number(s)..." value="{{ old('account_rep_phone') }}">
      </div>
      <div class="form-group">
        <label for="account_rep_email">Account Representative Email</label>
        <input type="email" class="form-control" name="account_rep_email" placeholder="account@rep.com" value="{{ old('account_rep_email') }}">
      </div>
      <div class="form-group">
        <label for="website">Company Website</label>
        <input type="text" class="form-control" name="website" placeholder="www.website.com" value="{{ old('website') }}">
      </div>
      <div class="form-group">
        <label for="subscribers">Dependants</label>
        <select class="form-control select2-multi" multiple="multiple" name="subscribers[]" placeholder="Add dependants">
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="subscriberHelp" class="form-text text-muted">
          Before you add dependants they have to be in the system already. 
          If they are not please add them <a href="{{ route('subscribers.create') }}">here.</a></small>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
      </div>
    </form>
  </div>
</div>

@stop

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}" rel="stylesheet"></script>

  <script type="text/javascript">
    $('.select2-multi').select2();
    $('.select2-multi').select2().val({!! json_encode($subscribers) !!}).trigger('change');
  </script>
@stop