@extends('main')

@section('title', '| Corporations')

@section('content')

<div class="row pb-80">
  <div class="col-md-3">
    <h2>Packages</h2>
    @include('partials._sidemenu')
  </div>
  <div class="col-md-9">
    <div class="d-flex justify-content-between align-items-baseline">
      <h2 class="mr-2">Corporate</h2>
      <h4>Group</h4>
    </div>
    <div class="d-flex justify-content-between flex-wrap mb-2">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $count }} Companies
        </span>
      </div>
      {{-- <div class="d-flex align-items-center badges-stats">
        <span class="badge badge-pill badge-warning mr-2">40 Due</span>
        <span class="badge badge-pill badge-danger mr-2">40 Over Due</span>
      </div> --}}
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th></th>
            <th scope="col">Name</th>
            <th scope="col">Group#</th>
            <th scope="col">Dependants</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($corporates as $corporate)
          <tr>
            @if(count($corporate->payments) > 0 && (new DateTime(date('Y-m-d', strtotime($corporate->payments[0]->start_date))))->diff(new DateTime(date('Y-m-d', strtotime($corporate->payments[0]->end_date))))->format('%m') == 1)
            <td class="text-dark"><i class="fas fa-circle"></i></td>
            @elseif(count($corporate->payments) > 0 && (round(strtotime($corporate->payments[0]->end_date) - time()) / (60 * 60 * 24)) > 30)
            <td class="text-primary"><i class="fas fa-circle"></i></td>
            @elseif(count($corporate->payments) > 0 && (new DateTime(date('Y-m-d', strtotime($corporate->payments[0]->end_date))))->diff(new DateTime(date('Y-m-d', strtotime(time()))))->format('%d') < 0)
            <td class="text-danger"><i class="fas fa-circle"></i></td>
            @elseif(count($corporate->payments) > 0 && (new DateTime(date('Y-m-d', strtotime($corporate->payments[0]->end_date))))->diff(new DateTime(date('Y-m-d', strtotime(time()))))->format('%d') < 31)
            <td class="text-warning"><i class="fas fa-circle"></i></td>
            @else
            <td><i class="far fa-circle"></i></td>
            @endif

            <td>{{ $corporate->name }}</td>
            <td>{{ Helper::rr_group_id($corporate->group_id) }}</td>
            <td>{{ $corporate->dependants }}</td>
            <td>
              <a href="{{ route('corporate.show', $corporate->id) }}"><i class="fas fa-arrow-right"></i></a>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

@stop