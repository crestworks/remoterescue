@extends('main')

@section('title', '| Administrators')

@section('content')

<div class="container mb-2">
  <div class="row pb-80">
    <div class="col-md-8 offset-md-2">
      <div class="d-flex justify-content-between mb-1">
        <h3>Admin Manager</h3>
        <div>
          <a href="#" class="icon-15"
            data-toggle="modal"
            data-target="#createAdminModal">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
      <div class="d-flex justify-content-between flex-wrap mb-3">
        <div class="d-flex align-items-center">
          <span class="badge badge-pill badge-primary">{{ $count }} Administrators</span>
        </div>
        <div class="d-flex align-items-center">
          <span class="badge badge-pill badge-light mr-2">{{ $masters }} Masters</span>
          <span class="badge badge-pill badge-light mr-2">{{ $viewers }} Viewers</span>
          <span class="badge badge-pill badge-light">{{ $editors }} Editors</span>
        </div>
      </div>
      <div class="table-responsive-md">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="pl-3">#</th>
              <th scope="pl-3">Name</th>
              <th scope="pl-3">Role</th>
              <th class="pl-3 pr-3" scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($admins as $admin)
            <tr>
              <td class="pl-3">{{ $admin->id }}</td>
              <td class="">{{ $admin->first }} {{ $admin->last }}</td>
              <td class="">{{ $admin->role->role }}</td>
              @if(Auth::user()->id == $admin->id)
              <td class="pl-3 pr-3 text-right">
                <a class="text-primary pr-3" href="{{ route('administrators.show', $admin->id) }}"><i class="fas fa-arrow-right"></i></a>
              </td>
              @else
              <td class="pl-3 pr-3 text-right">
                <a href="#"
                    class="text-primary pr-3"
                    data-toggle="modal"
                    data-target="#editAdminModal"
                    data-id="{{ $admin->id }}"
                    data-first="{{ $admin->first }}"
                    data-last="{{ $admin->last }}"
                    data-email="{{ $admin->email }}"
                    data-mobile="{{ $admin->mobile }}"
                    data-job_title="{{ $admin->job_title }}"
                    data-role_id="{{ $admin->role_id }}"
                    data-username="{{ $admin->username }}"
                    data-created="{{ date('M j, Y', strtotime($admin->created_at)) }}"
                    data-updated="{{ date('M j, Y', strtotime($admin->updated_at)) }}"
                    data-url="{{ url('administrators') }}">
                    <i class="far fa-eye"></i>
                </a>
              </td>
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Edit Admin Modal -->
<div class="modal fade" id="editAdminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <div class="d-flex align-items-baseline">
          <h5 class="modal-title" id="exampleModalLabel">Admin</h5>
          @if(Auth::user()->role_id < 2)
          <a href="#" id="admin-edit" class="ml-3"><i class="fas fa-edit"></i></a>
          @endif
        </div>
        <button id="close-edit-admin-modal" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-admin-form" action="{{ url('administrators', '1') }}" method="POST">

          {{ method_field('PUT') }}
          {{ csrf_field() }}

          <input id="admin-id" type="text" name="id" value="0" hidden>

          <div class="alert alert-info collapse" id="collapseInfo" role="alert">
            <strong>Please Note:</strong> Any changes that are made will only take effect once the <strong>"Update"</strong> button is clicked below.
            Otherwise your changes may be lost.
          </div>

          <div class="form-group">
            <label for="last">Last Name</label>
            <input readonly id="admin-last" type="text" class="form-control" name="last" value="">
          </div>
          <div class="form-group">
            <label for="first">First Name</label>
            <input readonly id="admin-first" type="text" class="form-control" name="first" value="">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input readonly id="admin-email" type="email" class="form-control" name="email" value="">
          </div>
          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input readonly id="admin-mobile" type="text" class="form-control" name="mobile" value="">
          </div>
          <div class="form-group">
            <label for="job_title">Job Title</label>
            <input readonly id="admin-job_title" type="text" class="form-control" name="job_title" value="">
          </div>
          <div class="form-group">
            <label for="role_id">Role</label>
            <select readonly id="admin-role_id"class="form-control" name="role_id">
              <option>Select</option>
              @foreach ($roles as $role)
              <option value="{{ $role->id }}">{{ $role->role }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="username">Username</label>
            <input readonly id="admin-username" type="text" class="form-control" value="">
          </div>
          <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div id="btn-cancel" class="btn-group" role="group" aria-label="{{ __('Cancel') }}">
              <button type="button" class="btn btn-light btn-block" data-dismiss="modal" aria-label="Close">{{ __('Cancel') }}</button>
            </div>
            <div id="btn-delete" class="btn-group mr-3" role="group" aria-label="{{ __('Delete') }}" hidden>
              <button type="button" class="btn btn-outline-danger btn-block" data-toggle="collapse" data-target="#collapseDelete" aria-expanded="false" aria-controls="collapseExample">{{ __('Delete') }}</button>
            </div>
            <div id="btn-update" class="btn-group" role="group" aria-label="{{ __('Update') }}" hidden>
              <button type="submit" class="btn btn-primary btn-block">{{ __('Update') }}</button>
            </div>
          </div>
        </form>
        <form class="alert alert-danger collapse mt-3 mb-0" id="collapseDelete" action="{{ url('admins', '1') }}" method="POST">

          {{ method_field('DELETE') }}
          {{ csrf_field() }}
  
          <p>You are about to delete this Administrator and all associated data. Are you sure you want to go ahead with this?</p>
          <input type="text" id="admin-delete-input" name="id" hidden>
          <button type="submit" class="btn btn-danger btn-block">{{ __('Confirm') }}</button>
        </form>
      </div>
      <div class="modal-footer d-flex justify-content-between">
        <small>Updated: <span id="admin-updated"></span></small>
        <small>Created: <span id="admin-created"></span></small>
      </div>
    </div>
  </div>
</div>

<!-- Create Admin Modal -->
<div class="modal fade" id="createAdminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ url('administrators') }}" method="POST">


          {{ csrf_field() }}
    
          <div class="form-group">
            <label for="first">First Name</label>
            <input type="text" class="form-control" name="first" placeholder="Winston">
          </div>
          <div class="form-group">
            <label for="last">Last Name</label>
            <input type="text" class="form-control" name="last" placeholder="Sizemore">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" placeholder="winston@ace-ambulance.com">
          </div>
          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input type="text" class="form-control" name="mobile" placeholder="0784123321">
          </div>
          <div class="form-group">
            <label for="job_title">Job Title</label>
            <input type="text" class="form-control" name="job_title" placeholder="Remote Rescue Subscription Manager">
          </div>
          <div class="form-group">
            <label for="group_type_id">Role</label>
            <select class="form-control" name="role_id">
              <option>Select</option>
              @foreach ($roles as $role)
              <option value="{{ $role->id }}">{{ $role->role }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" placeholder="wsizemore">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <div class="form-group input-group">
              <input type="password" class="form-control" name="password" placeholder="password" aria-describedby="button-addon3">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" id="button-addon3"><i class="far fa-eye"></i></button>
              </div>
            </div>
          </div>
          <div class="mt-3 d-flex justify-content-end">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
{{-- Dynamically display modal content --}}
<script type="text/javascript">

  $('#editAdminModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var actionURL = button.data('url')
    var adminId = button.data('id')
    var adminFirst = button.data('first')
    var adminLast = button.data('last')
    var adminEmail = button.data('email')
    var adminMobile = button.data('mobile')
    var adminJobTitle = button.data('job_title')
    var adminRoleID = button.data('role_id')
    var adminUsername = button.data('username')
    var adminCreated = button.data('created')
    var adminUpdated = button.data('updated') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('#admin-id').val(adminId)
    modal.find('#admin-first').val(adminFirst)
    modal.find('#admin-last').val(adminLast)
    modal.find('#admin-email').val(adminEmail)
    modal.find('#admin-mobile').val(adminMobile)
    modal.find('#admin-job_title').val(adminJobTitle)
    modal.find('#admin-role_id').val(adminRoleID)
    modal.find('#admin-username').val(adminUsername)
    modal.find('#admin-created').html(adminCreated)
    modal.find('#admin-updated').html(adminUpdated)
    modal.find('#admin-delete-input').val(adminId)
    
    modal.find('#edit-admin-form').attr('action', actionURL + '/' + adminId);
    modal.find('#collapseDelete').attr('action', actionURL + '/' + adminId);
  });

</script>

{{-- Edit Modal Form Content --}}
<script type="text/javascript">

  function editForm() {
    // Allow input edits
    $('#admin-first, #admin-last, #admin-email, #admin-mobile, #admin-job_title, #admin-role_id, #admin-username').attr("readonly", false);
  }

  function resetForm() {
    // Render input fields uneditable again
    $('#admin-first, #admin-last, #admin-email, #admin-mobile, #admin-job_title, #admin-role_id, #admin-username').attr("readonly", true);

    // Reset the form to original state
    // $("#edit-admin-form").trigger('reset');
  }

  function showButtons() {
    $('#btn-cancel').attr('hidden', true)
    $('#btn-delete').attr('hidden', false)
    $('#btn-update').attr('hidden', false)
  }

  function hideButtons() {
    $('#btn-cancel').attr('hidden', false)
    $('#btn-delete').attr('hidden', true)
    $('#btn-update').attr('hidden', true)
  }

  function handler1() {
    editForm()
    showButtons()
    $('#collapseInfo').collapse('show')
    $(this).one("click", handler2)
  }

  function handler2() {
    resetForm()
    hideButtons()
    $('#collapseInfo').collapse('hide')
    $('#collapseDelete').collapse('hide')
    $(this).one("click", handler1)
  }

  $("#admin-edit").one("click", handler1);

</script>
@stop