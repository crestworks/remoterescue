@extends('main')

@section('title', '| Show Administrator')

@section('content')

<div class="row pb-80">
  <div class="col-md-6 offset-md-3">
    <div class="d-flex justify-content-between mb-1">
      <div class="d-flex align-items-baseline">
        <h3>{{ $admin->first }} {{ $admin->last }}</h3>
        <a href="#" data-toggle="modal" data-target="#editAdminModal" class="ml-2"><i class="fas fa-edit"></i></a>
      </div>
      <div>
        <a href="{{ url('administrators') }}" class="icon-15" ><i class="fas fa-arrow-left"></i></a>
      </div>
    </div>
    
    <hr class="mb-1">

    <small class="">Last Updated: {{ date('M j, Y H:i', strtotime($admin->updated_at)) }}</small>
    
    <div class="mt-4">
      <div class="form-group">
        <label for="email">Email</label>
        <input readonly type="email" class="form-control" name="email" value="{{ $admin->email }}">
      </div>
      <div class="form-group">
        <label for="mobile">Mobile</label>
        <input readonly type="text" class="form-control" name="mobile" value="{{ $admin->mobile }}">
      </div>
      <div class="form-group">
        <label for="job_title">Job Title</label>
        <input readonly type="text" class="form-control" name="job_title" value="{{ $admin->job_title }}">
      </div>
      <div class="form-group">
        <label for="role_id">Role</label>
        <input readonly type="text" class="form-control" name="role_id" value="{{ $admin->role->role }}">
      </div>
      <div class="form-group">
        <label for="username">Username</label>
        <input readonly type="text" class="form-control" name="username" value="{{ $admin->username }}">
      </div>
    </div>

  </div>
</div>

<!-- Edit Admin Modal -->
<div class="modal fade" id="editAdminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Admin</h5>
        <button id="close-edit-admin-modal" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-admin-form" action="{{ url('administrators', $admin->id) }}" method="POST">

          {{ method_field('PUT') }}
          {{ csrf_field() }}

          <div class="alert alert-info" role="alert">
            <strong>Please Note:</strong> Any changes that are made will only take effect once the <strong>"Update"</strong> button is clicked below.
            Otherwise your changes may be lost.
          </div>

          <div class="form-group">
            <label for="last">Last Name</label>
            <input id="admin-last" type="text" class="form-control" name="last" value="{{ $admin->last }}">
          </div>
          <div class="form-group">
            <label for="first">First Name</label>
            <input id="admin-first" type="text" class="form-control" name="first" value="{{ $admin->first }}">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input id="admin-email" type="email" class="form-control" name="email" value="{{ $admin->email }}">
          </div>
          <div class="form-group">
            <label for="mobile">Mobile</label>
            <input  id="admin-mobile" type="text" class="form-control" name="mobile" value="{{ $admin->mobile }}">
          </div>
          <div class="form-group">
            <label for="job_title">Job Title</label>
            <input id="admin-job_title" type="text" class="form-control" name="job_title" value="{{ $admin->job_title }}">
          </div>
          <div class="form-group">
            <label for="role_id">Role</label>
            <select id="admin-role_id"class="form-control" name="role_id">
              <option>Select</option>
              @foreach ($roles as $role)
              <option {{ $role->id == $admin->role_id ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->role }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="username">Username</label>
            <input id="admin-username" type="text" class="form-control" name="username" value="{{ $admin->username }}">
          </div>
          
          <button type="submit" class="btn btn-primary btn-block">{{ __('Update') }}</button>
        
        </form>
      </div>
      <div class="modal-footer d-flex justify-content-between">
        <small>Updated: <span id="admin-updated"></span>{{ $admin->updated_at }}</small>
        <small>Created: <span id="admin-created"></span>{{ $admin->created_at }}</small>
      </div>
    </div>
  </div>
</div>

@stop