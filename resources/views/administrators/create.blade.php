@extends('main')

@section('title', '| Create Admin')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <h2>Create Admin</h2>

    <form action="{{ url('administrators') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="first">First Name</label>
        <input type="text" class="form-control" name="first" placeholder="Winston">
      </div>
      <div class="form-group">
        <label for="last">Last Name</label>
        <input type="text" class="form-control" name="last" placeholder="Sizemore">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" placeholder="winston@ace-ambulance.com">
      </div>
      <div class="form-group">
        <label for="mobile">Mobile</label>
        <input type="text" class="form-control" name="mobile" placeholder="0784123321">
      </div>
      <div class="form-group">
        <label for="job_title">Job Title</label>
        <input type="text" class="form-control" name="job_title" placeholder="Remote Rescue Subscription Manager">
      </div>
      <div class="form-group">
        <label for="group_type_id">Role</label>
        <select class="form-control" name="role_id">
          <option>Select</option>
          @foreach ($roles as $role)
          <option value="{{ $role->id }}">{{ $role->role }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" name="username" placeholder="wsizemore">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password">
      </div>
      
      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group col mr-2" role="group" aria-label="Cancel">
          <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light">Cancel</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Submit">
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
      </div>
    </form>

  </div>
</div>

@stop
