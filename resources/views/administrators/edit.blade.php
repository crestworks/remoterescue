@extends('main')

@section('title', '| Edit Administrator')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <h2>Edit Admin - {{ substr($admin->first, 0, 1) }}. {{ $admin->last }}</h2>
    <div class="d-flex justify-content-between mb-2">
      <small class="">Created: {{ date('M j, Y H:i', strtotime($admin->created_at)) }}</small>
      <small class="">Last Updated: {{ date('M j, Y H:i', strtotime($admin->updated_at)) }}</small>
    </div>
    <form action="{{ url('administrators', $admin->id) }}" method="POST">
    
      {{ method_field('PUT') }}
      {{ csrf_field() }}

      <div class="form-group">
        <label for="last">Last Name</label>
        <input type="text" class="form-control" name="last" value="{{ $admin->last }}">
      </div>
      <div class="form-group">
        <label for="last">First Name</label>
        <input type="text" class="form-control" name="first" value="{{ $admin->first }}">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" value="{{ $admin->email }}">
      </div>
      <div class="form-group">
        <label for="mobile">Mobile</label>
        <input type="text" class="form-control" name="mobile" value="{{ $admin->mobile }}">
      </div>
      <div class="form-group">
        <label for="job_title">Job Title</label>
        <input type="text" class="form-control" name="job_title" value="{{ $admin->job_title }}">
      </div>
      <div class="form-group">
        <label for="role_id">Role</label>
        <select class="form-control" name="role_id">
          <option>Select</option>
          @foreach ($roles as $role)
          <option {{ $admin->role_id == $role->id ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->role }}</option>
          @endforeach
        </select>
      </div>
      @if( Auth::user()->id == $admin->id )
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" name="username" value="{{ $admin->username }}">
      </div>
      @else
        <p>Because you are not the owner of this administrator account you do not have the ability to change the Username. If you would like to view the Username so you can remind the owner of the account please cancel and return to the View page.</p>
      @endif
      <div class=" d-flex pt-4">
        <a href="{{ url()->previous() }}" type="button" class="btn btn-stretch btn-light mr-2">Cancel</a>
        <button type="submit" class="btn btn-stretch btn-primary ml-2">Save</button>
      </div>
    </form>
  </div>
</div>

@stop