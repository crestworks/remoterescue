@extends('main')

@section('title', '| Group')

@section('content')

<div class="row pb-80">
  <div class="col-md-3">
    <h2>Packages</h2>
    @include('partials._sidemenu')
  </div>
  <div class="col-md-9">
    <div class="d-flex justify-content-between align-items-baseline">
      <h2 class="mr-2">Safari Camps</h2>
      <h4>Group</h4>
    </div>
    <div class="d-flex justify-content-between flex-wrap mb-2">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $count }} Groups
        </span>
      </div>
      {{-- <div class="d-flex align-items-center badges-stats">
        <span class="badge badge-pill badge-warning mr-2">40 Due</span>
        <span class="badge badge-pill badge-danger mr-2">40 Over Due</span>
      </div> --}}
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">Group Name</th>
            <th scope="col">Group#</th>
            <th scope="col">Members</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($groups as $group)
          <tr class="">
            <td>{{ $group->group_name }}</td>
            <td class="">{{ Helper::rr_group_id( $group->id) }}</td>
            <td>{{ $group->group_type->title }}</td>
            <td>
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('groups.show', $group->id) }}" class="btn btn-outline-dark btn-xs mr-2">View</a>
                <a href="{{ route('groups.edit', $group->id) }}" class="btn btn-outline-dark btn-xs">Edit</a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

@stop