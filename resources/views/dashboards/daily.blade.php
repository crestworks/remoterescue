@extends('main')

@section('title', '| Daily')

@section('content')

<div class="row pb-80">
  <div class="col-md-3">
    <h2>Packages</h2>
    @include('partials._sidemenu')
  </div>
  <div class="col-md-9">
    <h2 class="dashboard-title">Daily</h2>
    <div class="d-flex justify-content-between flex-wrap mb-2">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $count }} Subscribers
        </span>
      </div>
      <div class="d-flex align-items-center badges-stats">
        <span class="badge badge-pill badge-info mr-2">{{ $processing }} Processing</span>
        <span class="badge badge-pill badge-dark">{{ $fail }} Fail</span>
      </div>
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th></th>
            <th scope="col">Name</th>
            <th scope="col">Account#</th>
            <th scope="col">Vetting</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($subscribers as $subscriber)
          <tr>

            @if(count($subscriber->payments) > 0 && (new DateTime(date('Y-m-d', strtotime($subscriber->payments[0]->start_date))))->diff(new DateTime(date('Y-m-d', strtotime($subscriber->payments[0]->end_date))))->format('%m') == 1)
            <td class="text-dark"><i class="fas fa-circle"></i></td>
            @elseif(count($subscriber->payments) > 0 && (round(strtotime($subscriber->payments[0]->end_date) - time()) / (60 * 60 * 24)) > 30)
            <td class="text-primary"><i class="fas fa-circle"></i></td>
            @elseif(count($subscriber->payments) > 0 && (round(strtotime($subscriber->payments[0]->end_date) - time()) / (60 * 60 * 24)) < 0)
            <td class="text-danger"><i class="fas fa-circle"></i></td>
            @elseif(count($subscriber->payments) > 0 && (new DateTime(date('Y-m-d', strtotime($subscriber->payments[0]->end_date))))->diff(new DateTime(date('Y-m-d', strtotime(time()))))->format('%d') < 31)
            <td class="text-warning"><i class="fas fa-circle"></i></td>
            @else
            <td><i class="far fa-circle"></i></td>
            @endif

            <td class="">{{ $subscriber->first }} {{ $subscriber->last }}</td>
            <td class="">{{ Helper::rr_subscriber_id( $subscriber->id) }}</td>
            
            @if($subscriber->vetting > 0)
            <td class="text-success"><i class="fas fa-check-circle"></i></td>
            @elseif($subscriber->vetting < 0)
            <td class="text-info"><i class="fas fa-tasks"></i></td>
            @elseif($subscriber->vetting == 0)
            <td class="text-danger"><i class="fas fa-times-circle"></i></td>
            @endif
            
            <td>
              <a href="{{ route('subscribers.show', $subscriber->id) }}" class=""><i class="fas fa-arrow-right"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@stop