@extends('main')

@section('title', '| Group')

@section('content')

<div class="row pb-80">
  <div class="col-md-3">
    <h2>Packages</h2>
    @include('partials._sidemenu')
  </div>
  <div class="col-md-9">
    <h2 class="mr-2">Groups</h2>
    <div class="d-flex justify-content-between flex-wrap mb-2">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $count }} Groups
        </span>
      </div>
      {{-- <div class="d-flex align-items-center badges-stats">
        <span class="badge badge-pill badge-warning mr-2">40 Due</span>
        <span class="badge badge-pill badge-danger mr-2">40 Over Due</span>
      </div> --}}
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">Group Name</th>
            <th scope="col">Group#</th>
            <th scope="col">Group Type</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($groups as $group)
          <tr class="">
            <td>{{ $group->group_name }}</td>
            <td class="">{{ Helper::rr_group_id( $group->id) }}</td>
            <td>{{ $group->group_type->title }}</td>
            <td>
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('groups.show', $group->id) }}" class="btn btn-outline-dark btn-xs mr-2">View</a>
                <a href="{{ route('groups.edit', $group->id) }}" class="btn btn-outline-dark btn-xs">Edit</a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    <h2 class="mr-2">Group Members</h2>
    <div class="d-flex justify-content-between flex-wrap mb-2">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $count_subscribers }} Subscribers
        </span>
      </div>
      <div class="d-flex align-items-center badges-stats">
        {{-- <span class="badge badge-pill badge-warning mr-2">{{ $due }} Due</span> --}}
        {{-- <span class="badge badge-pill badge-danger mr-2">{{ $past }} Past</span> --}}
        <span class="badge badge-pill badge-info mr-2">{{ $processing }} Processing</span>
        <span class="badge badge-pill badge-dark">{{ $fail }} Fail</span>
      </div>
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Account#</th>
            <th scope="col">Vetting</th>
            <th class="btn-dashboard"></th>
          </tr>
        </thead>
        <tbody>

          @foreach($subscribers as $subscriber)
          <tr>
            <td class="">{{ $subscriber->first }} {{ $subscriber->last }}</td>
            <td class="">{{ Helper::rr_subscriber_id( $subscriber->id) }}</td>
            @if($subscriber->vetting > 0)
            <td><span class="badge badge-pill badge-success">Pass</span></td>
            @elseif($subscriber->vetting < 0)
            <td><span class="badge badge-pill badge-info">Processing</span></td>
            @elseif($subscriber->vetting == 0)
            <td><span class="badge badge-pill badge-dark">Fail</span></td>
            @endif
            <td class="">
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('subscribers.show', $subscriber->id) }}" class="btn btn-outline-dark btn-xs mr-2">View</a>
                <a href="{{ route('subscribers.edit', $subscriber->id) }}" class="btn btn-outline-dark btn-xs">Edit</a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@stop