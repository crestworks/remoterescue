@extends('main')

@section('title', '| Packages')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <div class="d-flex justify-content-between flex-wrap">
      <div class="d-flex align-items-center">
        <h2 class="mr-2">Group Types</h2>
      </div>
      <div class="d-flex align-items-center">
        <a href="{{ route('group_types.create') }}" class="btn btn-primary btn-sm">New</a>
      </div>
    </div>
    <div class="d-flex justify-content-between flex-wrap mb-3">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $active }} Active
        </span>
      </div>
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-secondary"> {{ $inactive }} Inactive</span>
      </div>
    </div>
    <p>Group Types fit in where a Family, Corporate, or Safari Camp subscription needs to be created. This table feeds into the selection process and was created because the programing behind the application calls for it, rather than the user experience requesting it.</p>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Active</th>
            <th  class="btn-dashboard"></th>
          </tr>
        </thead>
        <tbody>
          @foreach($group_types as $group_type)
          <tr>
            <th scope="row">{{ $group_type->id }}</th>
            <td>{{ $group_type->title }}</td>
            <td>{{ $group_type->active == "1" ? "True" : "False" }}</td>
            <td class="btn-toolbar-fix">
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('group_types.show', $group_type->id) }}" class="btn btn-outline-dark btn-xs mr-2">View</a>
                <a href="{{ route('group_types.edit', $group_type->id) }}" class="btn btn-outline-dark btn-xs">Edit</a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@stop