@extends('main')

@section('title', '| Create Package')

@section('content')

<div class="row">
  <div class="col-md-8 offset-md-2">
    <h2>Create Package</h2>

    <form action="{{ url('packages') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group individual">
        <label for="title">Package Title</label>
        <input type="text" class="form-control" name="title" placeholder="Keep the title brief">
      </div>
      <div class="form-group group">
        <label for="description">Description</label>
        <textarea type="text" class="form-control" name="description" placeholder="As much detail as possible..."></textarea>
      </div>
      <div class="form-group company">
        <label for="active">Active</label>
        <select type="text" class="form-control" name="active">
          <option value="1" selected>True</option>
          <option value="0">False</option>
        </select>
      </div>
      <div class="btn-toolbar">
        <div class="btn-group col mr-2" role="group" aria-label="Cancel">
          <a href="{{ route('packages.index') }}" type="button" class="btn btn-block btn-light">Cancel</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Submit">
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
      </div>
    </form>

  </div>
</div>

@stop

