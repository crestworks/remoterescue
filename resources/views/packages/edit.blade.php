@extends('main')

@section('title', '| Edit Package')

@section('content')

<div class="row">
  <div class="col-md-8 offset-md-2">
    <h2>Edit Package - {{ $package->title }}</h2>

    <form action="{{ url('packages', $package->id) }}" method="POST">

      {{ method_field('PUT') }}
      {{ csrf_field() }}

      <div class="form-group individual">
        <label for="title">Package Title</label>
        <input type="text" class="form-control" name="title" value="{{ $package->title }}">
      </div>
      <div class="form-group group">
        <label for="description">Description</label>
        <textarea type="text" class="form-control" name="description" placeholder="As much detail as possible...">{{ $package->description }}</textarea>
      </div>
      <div class="form-group company">
        <label for="active">Active</label>
        <select type="text" class="form-control" name="active">
          <option>Select</option>
          <option {{ $package->active == "1" ? 'selected' : '' }} value="1">True</option>
          <option {{ $package->active == "0" ? 'selected' : '' }} value="0">False</option>
        </select>
      </div>
      <div class="btn-toolbar">
        <div class="btn-group col mr-2" role="group" aria-label="Back">
          <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light">Back</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Update">
          <button type="submit" class="btn btn-block btn-primary">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>


@stop
