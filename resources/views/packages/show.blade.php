@extends('main')

@section('title', '| Packages')

@section('content')

<div class="row">
  <div class="col-md-8">
    <h2>Show Package - {{ $package->title }}</h2>

    <form>
      <div class="form-group individual">
        <label for="title">Package Title</label>
        <input readonly type="text" class="form-control" name="title" value="{{ $package->title }}">
      </div>
      <div class="form-group group">
        <label for="description">Description</label>
        <textarea readonly type="text" class="form-control" name="description" placeholder="As much detail as possible...">{{ $package->description }}</textarea>
      </div>
      <div class="form-group company">
        <label for="active">Active</label>
        @if ($package->active == "1")
        <input readonly type="text" class="form-control" name="active" value="True">
        @else
        <input readonly type="text" class="form-control" name="active" value="False">
        @endif
      </div>
      <div class="btn-toolbar">
        <div class="btn-group col" role="group" aria-label="Back">
          <a href="{{ route('packages.index') }}" type="button" class="btn btn-block btn-light">Back</a>
        </div>
      </div>
    </form>
  </div>

  <div class="col-md-4">
    <div class="card mb-2">
      <div class="card-body">
        <h5 class="card-title">Options</h5>
        <form>
          <div class="btn-toolbar">
            <div class="btn-group col mr-2" role="group" aria-label="Edit">
              <a href="{{ route('packages.edit', $package->id) }}" type="button" class="btn btn-block btn-primary">Edit</a>
            </div>
            <div class="btn-group col" role="group" aria-label="Delet">
              <a href="" type="button" class="btn btn-block btn-danger">Delete</a>
            </div>
          </div>
        </form>
      </div>
      <div class="card-footer text-muted">
        <small>Created: {{ date('M j, Y H:i', strtotime($package->created_at)) }}</small><br>
        <small>Updated: {{ date('M j, Y H:i', strtotime($package->updated_at)) }}</small>
      </div>
    </div>
  </div>
</div>

@stop
