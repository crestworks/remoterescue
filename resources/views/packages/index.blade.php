@extends('main')

@section('title', '| Packages')

@section('content')
<div class="container mb-2">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="d-flex justify-content-between mb-1">
        <h3>Packages</h3>
        <div>
          <a href="#" class="icon-15"
            data-toggle="modal"
            data-target="#createPackageModal">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
      <div class="d-flex justify-content-between flex-wrap mb-4">
        <div class="d-flex align-items-center">
          <span class="badge badge-pill badge-primary">{{ $active }} Active
          </span>
        </div>
        <div class="d-flex align-items-center">
          <span class="badge badge-pill badge-secondary"> {{ $inactive }} Inactive</span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container mb-4">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="table-responsive-sm">
        <table class="table table-sm table-striped">
          <thead>
            <tr>
              <th class="pl-3" scope="col"></th>
              <th class="pl-3" scope="col">Title</th>
              <th class="pl-3" scope="col">Active</th>
              <th class="pl-3 pr-3" scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($packages as $package)
            <tr>
              <td class="pl-3">{{ $package->id }}</td>
              <td class="pl-3">{{ $package->title }}</td>
              <td class="pl-3">{{ $package->active == "1" ? "True" : "False" }}</td>
              <td class="pl-3 pr-3 text-right">
                <a href="#"
                    class="text-primary pr-3"
                    data-toggle="modal"
                    data-target="#editPackageModal"
                    data-id="{{ $package->id }}"
                    data-title="{{ $package->title }}"
                    data-description="{{ $package->description }}"
                    data-active="{{ $package->active }}"
                    data-created="{{ date('M j, Y', strtotime($package->created_at)) }}"
                    data-updated="{{ date('M j, Y', strtotime($package->updated_at)) }}"
                    data-url="{{ url('packages') }}">
                    <i class="far fa-eye"></i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div> 
  </div>
</div>

<!-- Edit Package Modal -->
<div class="modal fade" id="editPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <div class="d-flex align-items-baseline">
          <h5 class="modal-title" id="exampleModalLabel">Package</h5>
          @if(Auth::user()->role_id < 3)
          <a href="#" id="package-edit" class="ml-3"><i class="fas fa-edit"></i></a>
          @endif
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-package-form" action="{{ url('package', '1') }}" method="POST">

          {{ method_field('PUT') }}
          {{ csrf_field() }}

          <input id="package-id" type="text" name="id" value="0" hidden>

          <div class="alert alert-info collapse" id="collapseInfo" role="alert">
            <strong>Please Note:</strong> Any changes that are made will only take effect once the <strong>"Update"</strong> button is clicked below.
            Otherwise your changes may be lost.
          </div>

          <div class="form-group">
            <label for="title">Title</label>
            <input readonly id="package-title" type="text" class="form-control" name="title" placeholder="Name of package" value="">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea readonly id="package-description" class="form-control" name="description" placeholder="Package description..."></textarea>
          </div>
          <div class="form-group company">
            <label for="active">Active</label>
            <select readonly id="package-active" class="form-control" name="active">
              <option>Select</option>
              <option value="1">True</option>
              <option value="0">False</option>
            </select>
          </div>
          <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div id="btn-cancel" class="btn-group" role="group" aria-label="{{ __('Cancel') }}">
              <button type="button" class="btn btn-light btn-block" data-dismiss="modal" aria-label="Close">{{ __('Cancel') }}</button>
            </div>
            <div id="btn-delete" class="btn-group mr-3" role="group" aria-label="{{ __('Delete') }}" hidden>
              <button type="button" class="btn btn-outline-danger btn-block" data-toggle="collapse" data-target="#collapseDelete" aria-expanded="false" aria-controls="collapseExample">{{ __('Delete') }}</button>
            </div>
            <div id="btn-update" class="btn-group" role="group" aria-label="{{ __('Update') }}" hidden>
              <button type="submit" class="btn btn-primary btn-block">{{ __('Update') }}</button>
            </div>
          </div>
        </form>

        <form class="alert alert-danger collapse mt-3 mb-0" id="collapseDelete" action="{{ url('package', '1') }}" method="POST">

          {{ method_field('DELETE') }}
          {{ csrf_field() }}
  
          <p>You are about to delete this Package and all associated data. Are you sure you want to go ahead with this?</p>
          <input type="text" id="package-delete-input" name="id" hidden>
          <button type="submit" class="btn btn-danger btn-block">{{ __('Confirm') }}</button>
        </form>
      </div>
      <div class="modal-footer d-flex justify-content-between">
        <small>Updated: <span id="package-updated"></span></small>
        <small> Created: <span id="package-created"></span></small>
      </div>
    </div>
  </div>
</div>

<!-- Create Package Modal -->
<div class="modal fade" id="createPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Package</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="create-role-form" action="{{ url('packages') }}" method="POST">

          {{ csrf_field() }}

          <div class="form-group">
            <label for="Title">Title</label>
            <input type="text" class="form-control" name="title" placeholder="Package Title">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" placeholder="Package description..."></textarea>
          </div>
          <div class="form-group">
          <label for="active">Active</label>
            <select class="form-control" name="active">
              <option>Select</option>
              <option value="1">True</option>
              <option value="0">False</option>
            </select>
          </div>
          <div class="mt-3 d-flex justify-content-end">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
{{-- Dynamically display modal content --}}
<script type="text/javascript">
  $('#editPackageModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var actionURL = button.data('url')
    var packageId = button.data('id')
    var packageTitle = button.data('title')
    var packageDescription = button.data('description')
    var packageCreated = button.data('created')
    var packageUpdated = button.data('updated')
    var packageActive = button.data('active') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('#package-id').val(packageId)
    modal.find('#package-title').val(packageTitle)
    modal.find('#package-description').val(packageDescription)
    modal.find('#package-created').html(packageCreated)
    modal.find('#package-updated').html(packageUpdated)
    modal.find('#package-active').val(packageActive)
    modal.find('#package-delete-input').val(packageId)
    
    modal.find('#edit-package-form').attr('action', actionURL + '/' + packageId);
    modal.find('#collapseDelete').attr('action', actionURL + '/' + packageId);
  });

</script>

{{-- Edit Modal Form Content --}}
<script type="text/javascript">

  function editForm() {
    // Allow input edits
    $('#package-title, #package-description, #package-active').attr("readonly", false);
  }

  function resetForm() {
    // Render input fields uneditable again
    $('#package-title, #package-description, #package-active').attr("readonly", true);

    // Reset the form to original state
    $("#edit-package-form").trigger('reset');
  }

  function showButtons() {
    $('#btn-cancel').attr('hidden', true)
    $('#btn-delete').attr('hidden', false)
    $('#btn-update').attr('hidden', false)
  }

  function hideButtons() {
    $('#btn-cancel').attr('hidden', false)
    $('#btn-delete').attr('hidden', true)
    $('#btn-update').attr('hidden', true)
  }

  function handler1() {
    editForm()
    showButtons()
    $('#collapseInfo').collapse('show')
    $(this).one("click", handler2)
  }

  function handler2() {
    resetForm()
    hideButtons()
    $('#collapseInfo').collapse('hide')
    $(this).one("click", handler1)
  }

  $("#package-edit").one("click", handler1);

</script>
@stop