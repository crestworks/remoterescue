@extends('main')

@section('title', '| Admin Reset Password')

@section('content')

<div class="row mb-80">
  <div class="col-md-4 offset-md-4">
    <div class="card">
      <h5 class="card-header">Admin - Reset Password</h5>

      <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="" role="form" method="POST" action="{{ route('admin.password.email') }}">
          
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label">E-Mail Address</label>
            <div class="">
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

              @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <button type="submit" class="btn btn-block btn-primary">Send Password Reset Link</button>
          <a class="btn btn-link" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> Back</a>

        </form>
      </div>
    </div>
  </div>
</div>
@stop
