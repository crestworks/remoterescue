@extends('main')

@section('title', '| Admin Login')

@section('content')

<div class="row mb-80">
  <div class="col-md-4 offset-md-4">
    <div class="card">
      <h5 class="card-header">Admin Login</h5>

      <div class="card-body">
        <form method="POST" action="{{ route('admin.login.submit') }}">
          
            {{ csrf_field() }}

          <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="username">Username</label>

            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

            @if ($errors->has('username'))
            <span class="help-block">
              <strong>{{ $errors->first('username') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">Password</label>

            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
              </label>
            </div>
          </div>

          <button type="submit" class="btn btn-primary btn-block">Login</button>

          <a class="btn btn-link" href="{{ route('admin.password.request') }}"> Forgot Your Password?</a>
        </form>
      </div>
    </div>
  </div>
</div>
@stop
