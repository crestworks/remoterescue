<!doctype html>
<html lang="en">

	@include('partials._head')

  <body>

  	@include('partials._nav')

    <main class="container">

      @include('partials._messages')

			@yield('content')

		</main>

    @include('partials._footer')

  </body>
</html>