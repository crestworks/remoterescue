@extends('main')

@section('title', '| Edit Family')

@section('stylesheets')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')

<div class="row pb-80">
  <div class="col-md-6 offset-md-3">
    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Edit Family</h3>
      <a href="{{ url()->previous() }}" class="btn-icon-primary lead"><i class="fas fa-arrow-left"></i></a>
    </div>
    <hr>
    <form action="{{ url('family', $family->id) }}" method="POST">

      {{ method_field('PUT') }}
      {{ csrf_field() }}

      <div class="form-group">
        <label for="name">Family Name</label>
        <input type="text" class="form-control" name="name" placeholder="Use the surname" value="{{ $family->name }}">
      </div>
      <div class="form-group">
        <label for="subscribers">Dependants</label>
        <select class="form-control select2-multi" multiple="multiple" name="subscribers[]" placeholder="Add dependants">
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
        <small id="subscriberHelp" class="form-text text-muted">Before you add dependants they have to be in the system already. 
          If they are not please add them <a href="{{ route('subscribers.create') }}">here.</a></small>
      </div>
      <div class="form-group">
        <label for="primary">Primary</label>
        <select class="form-control" name="primary">
          <option>Select a primary</option>
          @foreach ($subscribers as $subscriber)
          <option {{ $family->primary == $subscriber->id ? 'selected' : '' }} value="{{ $subscriber->id }}">
            {{ $subscriber->first }} {{ $subscriber->last }}
          </option>
          @endforeach
        </select>
        <small id="subscriberHelp" class="form-text text-muted">You must only select a Primary from the list of Family Dependants.</small>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
      </div>
    </form>
  </div>
</div>

@stop

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}" rel="stylesheet"></script>

  <script type="text/javascript">
    $('.select2-multi').select2();
    $('.select2-multi').select2().val({!! json_encode($family->subscribers()->allRelatedIds()) !!}).trigger('change');
  </script>
@stop