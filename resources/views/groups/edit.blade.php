@extends('main')

@section('title', '| Edit Group')

@section('stylesheets')
  <link href="http://localhost:8000/css/select2.min.css" rel="stylesheet" type="text/css">
@stop

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <h2>Edit Group</h2>

    <form action="{{ url('groups', $group->id) }}" method="POST">

      {{ method_field('PUT') }}
      {{ csrf_field() }}

      <div class="form-group">
        <label for="accountNumber">Account Number</label>
        <input readonly type="text" class="form-control" name="accountNumber" aria-describedby="accountNumber" value="{{ Helper::rr_group_id( $group->id) }}">
      </div>
      <div class="form-group">
        <label for="group_name">Group Name</label>
        <input type="text" class="form-control" name="group_name" value="{{ $group->group_name }}" placeholder="Enter group name eg. Wilson Family">
      </div>
      <div class="form-group">
        <label for="group_type_id">Group Type</label>
        <select class="form-control" name="group_type_id">
          <option>Select</option>
          @foreach ($group_types as $group_type)
          <option {{ $group->group_type_id == $group_type->id ? 'selected' : '' }} value="{{ $group_type->id }}">{{ $group_type->title }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="subscribers">Dependants</label>
        <select class="form-control select2-multi" multiple="multiple" name="subscribers[]">
          <option>Select</option>
          @foreach ($subscribers as $subscriber)
          <option value="{{ $subscriber->id }}">{{ $subscriber->first }} {{ $subscriber->last }}</option>
          @endforeach
        </select>
      </div>
      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group col mr-2" role="group" aria-label="Cancel">
          <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light">Back</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Submit">
          <button type="submit" class="btn btn-block btn-primary">Update</button>
        </div>
      </div>
    </form>

  </div>
</div>

@stop

@section('scripts')
  <script type="text/javascript" src="http://localhost:8000/js/select2.min.js" rel="stylesheet"></script>

  <script type="text/javascript">
    $('.select2-multi').select2();
    $('.select2-multi').select2().val({!! json_encode($group->subscribers()->allRelatedIds()) !!}).trigger('change');
  </script>
@stop