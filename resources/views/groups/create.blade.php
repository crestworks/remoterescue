@extends('main')

@section('title', '| Create Group')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <h2>Create Group</h2>

    <form action="{{ url('groups') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="group_name">Group Name</label>
        <input type="text" class="form-control" name="group_name" placeholder="Enter group name eg. Wilson Family">
      </div>
      <div class="form-group">
        <label for="group_type_id">Group Type</label>
        <select class="form-control" name="group_type_id">
          <option>Select</option>
          @foreach ($group_types as $group_type)
          <option value="{{ $group_type->id }}">{{ $group_type->title }}</option>
          @endforeach
        </select>
      </div>
      
      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group col mr-2" role="group" aria-label="Cancel">
          <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light">Cancel</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Submit">
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
      </div>
    </form>

  </div>
</div>

@stop
