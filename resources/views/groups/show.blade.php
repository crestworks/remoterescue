@extends('main')

@section('title', '| Show Group')

@section('content')

<div class="row pb-80">
  <div class="col-md-8">
    <h2>Group - {{ Helper::rr_group_id($group->id) }}</h2>

    <div>
      <div class="form-group">
        <label for="group_name">Group Name</label>
        <input readonly type="text" class="form-control" name="group_name" value="{{ $group->group_name }}">
      </div>
      <div class="form-group">
        <label for="group_type">Group Type</label>
        <input readonly type="text" class="form-control" name="group_type" value="{{ $group->group_type->title }}">
      </div>
    </div>

    <div class="d-flex justify-content-between">
      <h3>Dependants</h3>
      <h3><i class="fas fa-users"></i> {{ count($group->subscribers) }}</h3>
    </div>
    @if(count($group->subscribers) > 0)
    <small>If the number of dependants is greater than that expicitly indicated (above), the extra dependants will be highlighted.</small>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Principle</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($group->subscribers as $dependant)
          <tr>
            <td>{{ $dependant->first }}</td>
            <td>{{ $dependant->last }}</td>
            <td><span class="badge badge-pill badge-primary mr-2">P</span></td>
            <td>
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('subscribers.show', $dependant->id) }}" class="btn btn-outline-dark btn-xs mr-2">View</a>
                {{-- <button type="button" class="btn btn-outline-dark btn-xs">Remove</button> --}}
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    @else
    <div class="card bg-light mb-3">
      <div class="card-body">
        There are currently no dependants for this Group.
      </div>
    </div>
    @endif

    <div class="d-flex justify-content-between">
      <h3>Comments</h3>
      <h3><i class="fas fa-comments"></i> {{ count($group->comment_groups) }}</h3>
    </div>
    @if(count($group->comment_groups) > 0)
    @foreach($group->comment_groups as $comment)
    <div class="card mb-2">
      <div class="card-body">
        <p class="card-text">{{ $comment->comment }}</p>
        <div class="d-flex justify-content-between">
          <p class="comment-name">- Author</p>
          <button class="btn btn-xs btn-secondary" data-toggle="modal" data-target="#modal{{ $comment->id }}">Edit Comment</button>
        </div>
      </div>
      <div class="card-footer text-muted d-flex justify-content-between">
        <small>Created: {{ date('M j, Y H:i', strtotime($comment->created_at)) }}</small>
        <small>Updated: {{ date('M j, Y H:i', strtotime($comment->updated_at)) }}</small>
      </div>
    </div>
    <!-- Modal - edit this comment-->
    <div class="modal fade" id="modal{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form class="modal-content" action="{{ url('comment_groups', $comment->id) }}" method="POST">

          {{ method_field('PUT') }}
          {{ csrf_field() }}

          <div class="modal-header bg-light">
            <h5 class="modal-title" id="exampleModalLabel">Edit Comment Below</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body comment-modal-edit">
            <textarea rows="5" class="form-control comment-form-edit" name="comment">{{ $comment->comment }}</textarea>
          </div>
          <div class="modal-footer bg-light">
            {{-- <button type="button" class="btn btn-xs  btn-light" data-dismiss="modal">Delete</button> --}}
            <button type="submit" class="btn btn-xs btn-primary">Save</button>
          </div>

        </form>
      </div>
    </div>
    @endforeach
    @else 
    <div class="card bg-light mb-3">
      <div class="card-body">
        There are currently no Comments for this Group.
      </div>
    </div>
    @endif

    {{-- <h3>Payments</h3>
    <small>The Payments table is termined by the group or company the Subscriber is associated with, if any. If the Subscriber does not belong to a group the User payments will be displayed here.</small>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Payment Period</th>
          <th scope="col">Amount</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>1 Jun 2018 to 31 Jun 2018</td>
          <td>$ 60.00</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>1 May 2018 to 31 May 2018</td>
          <td>$ 60.00</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>1 Apr 2018 to 31 Apr 2018</td>
          <td>$ 60.00</td>
        </tr>
      </tbody>
    </table> --}}

  </div>
  <div class="col-md-4">

    <div class="card mb-2">
      <div class="card-body">
        <h4 class="card-title">Group Details</h4>
        <p class="card-subtitle mb-3">Group ID: {{ Helper::rr_group_id($group->id) }}</p>
        <form>
          <h5 class="card-title">Group Principle</h5>
          <div class="form-group">
            <select class="form-control" id="add">
              @foreach($group->subscribers as $dependant)
              <option>{{ $dependant->first }} {{ $dependant->last }}</option>
              @endforeach
            </select>
          </div>
          <button type="button" class="btn btn-block btn-primary">Change</button>
        </form>
      </div>
      <div class="card-footer text-muted">
        <small>Created: {{ date('M j, Y H:i', strtotime($group->created_at)) }}</small><br>
        <small>Updated: {{ date('M j, Y H:i', strtotime($group->updated_at)) }}</small>
      </div>
    </div>

    <div class="card mb-2">
      <div class="card-body">
        <h4 class="card-title">Admin Tools</h4>
        <p class="card-text">Please update Group information regularly so that we are always able to contact in case of emergencies.</p>
        <a href="{{ route('groups.edit', $group->id) }}" type="button" class="mb-3 btn btn-block btn-primary">Update Group</a>
        <h5 class="card-title">Add Comment</h5>
        <form class="mt-3 mb-3" action="{{ url('comment_groups', $group->id) }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            {{-- <label for="last">Add Comment</label> --}}
            <textarea rows="10" class="form-control" name="comment"></textarea>
          </div>
          <button type="submit" class="btn btn-block btn-primary">Comment</button>
        </form>
          
        {{-- <h5 class="card-title">Add Payment</h5>

        <form class="mt-3 mb-3" action="{{ url('payments', $group->id) }}" method="POST">
          {{ csrf_field() }}
          
          <div class="form-group">
            <label for="start_date">Start Date</label>
            <input type="date" class="form-control" name="start_date" placeholder="MM/DD/YYYY">
          </div>
          <div class="form-group">
            <label for="amount">Amount</label>
            <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon3">$</span>
              </div>
              <input type="text" class="form-control" placeholder="108.00" name="amount" aria-label="weight" aria-describedby="basic-addon3">
            </div>
          </div>
          <div class="form-group">
            <label for="paid_date">Date Paid</label>
            <input type="date" class="form-control" name="paid_date" placeholder="MM/DD/YYYY">
          </div>
          <button type="submit" class="btn btn-block btn-primary">Payment</button>
        </form> --}}

        <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light mt-2">Back</a>
      </div>
    </div>

  </div>
</div>

@stop
