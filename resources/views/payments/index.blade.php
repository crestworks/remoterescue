@extends('main')

@section('title', '| Payments')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <div class="title-section d-flex justify-content-between align-items-baseline">
      <h2>Payments</h2>
      <div class="title-section-btn-group">
        <a href="{{ url($makers, $entity->id) }}" class="btn-icon-primary lead"><i class="fas fa-arrow-left"></i></a>
      </div>
    </div>

    <p>
      Please be aware that when you update payments the payment algorithm 
      uses the current number of dependants. 
      This means if the number of dependants has increase over time 
      and you realise a past payment was incorrect you may further 
      mess things up. 
      If you have to edit payments you must first confirm with the developer.
    </p>

    @if(count($payments) > 0)
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">Amount</th>
            <th scope="col">Dependants</th>
            <th scope="col">Paid Date</th>
            <th scope="col">Period End</th>
            <th></th>
          </tr>
        </thead>
        <tbody>

          @foreach($payments as $payment)
          <tr>
            <td>${{ $payment->amount }}</td>
            <td>{{ $payment->dependants }}</td>
            <td>{{ date('M j, Y', strtotime($payment->paid_date)) }}</td>
            <td>{{ date('M j, Y', strtotime($payment->end_date)) }}</td>
            <td>
              <div class="btn-toolbar justify-content-end">
                <a class="mr-2"
                    href="#"
                    data-toggle="modal"
                    data-target="#editPaymentModal"
                    data-id="{{ $id }}"
                    data-payment-id="{{ $payment->id }}"
                    data-payment-maker="{{ $maker }}"
                    data-url="{{ url('payment') }}"
                    data-amount="{{ $payment->amount }}"
                    data-paid-date="{{ $payment->paid_date }}"><i class="fas fa-pencil-alt"></i>
                </a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    @else
    <div class="card bg-light mb-3">
      <div class="card-body">
        There are currently no Payments entered.
      </div>
    </div>
    @endif

    <!-- Edit Payment Modal -->
    <div class="modal fade" id="editPaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          
    
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Payment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form id="edit-payment-form" action="{{ url('payment', '0', '0') }}" method="POST">
  
              {{ method_field('PUT') }}
              {{ csrf_field() }}

              <input id="payment-id" type="text" name="id" value="0" hidden>
              <div class="form-group">
                <label for="amount">Payment Amount</label>
                <input type="text" id="payment-amount-input" class="form-control" name="amount" placeholder="99.99">
              </div>
              <div class="form-group">
                <label for="paid_date">Date Paid</label>
                <input type="date" id="payment-paid-date-input" class="form-control" name="paid_date" placeholder="MM/DD/YYYY">
              </div>

              <div class="mt-3 d-flex justify-content-between">
                <button {{ Auth::user()->role_id == 1 ? "" : "disabled" }} class="btn btn-outline-danger" type="button" data-toggle="collapse" data-target="#delete-payment-form" aria-expanded="false" aria-controls="delete-payment-form">
                  Delete
                </button>
                <div>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
              </div>
            </form>

            @if(Auth::user()->role_id == 1)
            <form class="alert alert-danger collapse mt-3 mb-0" id="delete-payment-form" action="{{ url('payment', '0', '0') }}" method="POST">
  
              {{ method_field('DELETE') }}
              {{ csrf_field() }}
      
              <p>You are about to delete this Payment and all associated data. Are you sure you want to go ahead with this?</p>
              <input type="text" id="payment-delete-input" name="id" hidden>
              <button type="submit" class="btn btn-danger btn-block">Confirm</button>
            </form>
            @endif

          </div>
        </div>
      </div>
    </div>

  </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">

  $('#editPaymentModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var actionURL = button.data('url')
    var id = button.data('id')
    var paymentMaker = button.data('payment-maker')
    var paymentId = button.data('payment-id')
    var paymentAmount = button.data('amount')
    var paymentPaidDate = button.data('paid-date') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('#payment-id').val(paymentId)
    modal.find('#payment-delete-input').val(paymentId)
    modal.find('#payment-amount-input').val(paymentAmount)
    modal.find('#payment-paid-date-input').val(paymentPaidDate)
    modal.find('#edit-payment-form').attr('action', actionURL + '/' + paymentId + '/' + paymentMaker + '/' + id)
    modal.find('#delete-payment-form').attr('action', actionURL + '/' + paymentId + '/' + paymentMaker + '/' + id)

    // modal.find('#edit-comment-form').action = actionURL + '/' + commentId
    // alert(actionURL + '/' + commentId)
  });

</script>
@stop
