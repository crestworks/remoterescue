@extends('main')

@section('title', '| Search Results')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <div class="d-flex justify-content-between flex-wrap">
      <div class="d-flex align-items-center">
        <h2 class="mr-2">Search: "{{ $query }}"</h2>
      </div>
      <div class="d-flex align-items-center">
        <p>{{ $count }} Results</p>
      </div>
    </div>
    @if($count > 0)
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Account #</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($results as $subscriber)
          <tr>
            <td>{{ $subscriber->first }} {{ $subscriber->last }}</td>
            <td>{{ Helper::rr_subscriber_id($subscriber->id) }}</td>
            <td>
              <a href="{{ route('subscribers.show', $subscriber->id) }}"><i class="fas fa-arrow-right"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @else
    <div>
      <p>No details were found matching your citeria.</p>
    </div>
    @endif
  </div>
</div>

@stop