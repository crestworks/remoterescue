@extends('main')

@section('title', '| Questions')

@section('content')
<div class="container mb-2">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="d-flex justify-content-between mb-1">
        <h3>Medical Questions</h3>
        <div>
          <a href="#" class="icon-15"
            data-toggle="modal"
            data-target="#createQuestionModal">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
      <div class="d-flex justify-content-between flex-wrap mb-4">
        <div class="d-flex align-items-center">
          <span class="badge badge-pill badge-primary">{{ $active }} Active
          </span>
        </div>
        <div class="d-flex align-items-center">
          <span class="badge badge-pill badge-secondary"> {{ $inactive }} Inactive</span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container mb-4">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="table-responsive-sm">
        <table class="table table-sm table-striped">
          <thead>
            <tr>
              <th scope="pl-3">#</th>
              <th scope="pl-3">Question</th>
              <th scope="pl-3">Active</th>
              <th class="pl-3 pr-3" scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($questions as $question)
            <tr>
              <th>{{ $question->id }}</th>
              <td>{{ $question->question }}</td>
              <td>{{ $question->active == "1" ? "True" : "False" }}</td>
              <td class="pl-3 pr-3 text-right">
                <a href="#"
                    class="text-primary pr-3"
                    data-toggle="modal"
                    data-target="#editQuestionModal"
                    data-id="{{ $question->id }}"
                    data-question="{{ $question->question }}"
                    data-expected="{{ $question->expected }}"
                    data-active="{{ $question->active }}"
                    data-created="{{ date('M j, Y', strtotime($question->created_at)) }}"
                    data-updated="{{ date('M j, Y', strtotime($question->updated_at)) }}"
                    data-url="{{ url('questions') }}">
                    <i class="far fa-eye"></i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div> 
  </div>
</div>

<!-- Edit Question Modal -->
<div class="modal fade" id="editQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <div class="d-flex align-items-baseline">
          <h5 class="modal-title" id="exampleModalLabel">Question</h5>
          @if(Auth::user()->role_id < 3)
          <a href="#" id="question-edit" class="ml-3"><i class="fas fa-edit"></i></a>
          @endif
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit-question-form" action="{{ url('question', '1') }}" method="POST">

          {{ method_field('PUT') }}
          {{ csrf_field() }}

          <input id="question-id" type="text" name="id" value="0" hidden>

          <div class="alert alert-info collapse" id="collapseInfo" role="alert">
            <strong>Please Note:</strong> Any changes that are made will only take effect once the <strong>"Update"</strong> button is clicked below.
            Otherwise your changes may be lost.
          </div>

          <div class="form-group">
            <label for="question">Question</label>
            <textarea readonly id="question-question" type="text" class="form-control" name="question" aria-describedby="question" placeholder="Question goes here... ">{{ $question->question }}</textarea>
            <small>By updating this Question you are also updating the name associated with the question.</small>
            <small>Remember to end it with a question mark (?) and that it must be a yes/no question.</small>
          </div>
          <div class="form-group">
            <label for="expected">Expected Answer</label>
            <select readonly id="question-expected" class="form-control" name="expected">
              <option>Select</option>
              <option value="1">Yes</option>
              <option value="0">No</option>
            </select>
          </div>
          <div class="form-group">
            <label for="active">Active</label>
            <select readonly id="question-active" class="form-control" name="active">
              <option>Select</option>
              <option value="1">True</option>
              <option value="0">False</option>
            </select>
          </div>

          <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div id="btn-cancel" class="btn-group" role="group" aria-label="{{ __('Cancel') }}">
              <button type="button" class="btn btn-light btn-block" data-dismiss="modal" aria-label="Close">{{ __('Cancel') }}</button>
            </div>
            <div id="btn-delete" class="btn-group mr-3" role="group" aria-label="{{ __('Delete') }}" hidden>
              <button type="button" class="btn btn-outline-danger btn-block" data-toggle="collapse" data-target="#collapseDelete" aria-expanded="false" aria-controls="collapseExample">{{ __('Delete') }}</button>
            </div>
            <div id="btn-update" class="btn-group" role="group" aria-label="{{ __('Update') }}" hidden>
              <button type="submit" class="btn btn-primary btn-block">{{ __('Update') }}</button>
            </div>
          </div>
        </form>

        <div class="alert alert-danger collapse mt-3 mb-0" id="collapseDelete">
          <p>Questions cannot be deleted. 
            This is beacause all associated data will be lost effecting many profiles.</p>
        </div>
      </div>
      <div class="modal-footer d-flex justify-content-between">
        <small>Updated: <span id="question-updated"></span></small>
        <small> Created: <span id="question-created"></span></small>
      </div>
    </div>
  </div>
</div>

<!-- Create Question Modal -->
<div class="modal fade" id="createQuestionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Question</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ url('questions') }}" method="POST">

          {{ csrf_field() }}
    
          <div class="form-group">
            <label for="question">Question</label>
            <textarea type="text" class="form-control" name="question" aria-describedby="question" placeholder="Question goes here... "></textarea>
            <small>Remember to end it with a question mark (?) and that it must be a yes/no question</small>
          </div>
          <div class="form-group">
            <label for="expected">Expected Answer</label>
            <select class="form-control" name="expected">
              <option>Select</option>
              <option value="1">YES</option>
              <option value="0">NO</option>
            </select>
          </div>
          <div class="form-group">
            <label for="active">Active</label>
            <select class="form-control" name="active">
              <option>Select</option>
              <option value="1">True</option>
              <option value="0">False</option>
            </select>
          </div>
          <div class="mt-3 d-flex justify-content-end">
            <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
{{-- Dynamically display modal content --}}
<script type="text/javascript">
  $('#editQuestionModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var actionURL = button.data('url')
    var questionId = button.data('id')
    var questionQuestion = button.data('question')
    var questionExpected = button.data('expected')
    var questionActive = button.data('active')
    var questionCreated = button.data('created')
    var questionUpdated = button.data('updated') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('#question-id').val(questionId)
    modal.find('#question-question').val(questionQuestion)
    modal.find('#question-expected').val(questionExpected)
    modal.find('#question-active').val(questionActive)
    modal.find('#question-created').html(questionCreated)
    modal.find('#question-updated').html(questionUpdated)
    
    modal.find('#edit-question-form').attr('action', actionURL + '/' + questionId);
  });

</script>

{{-- Edit Modal Form Content --}}
<script type="text/javascript">

  function editForm() {
    // Allow input edits
    $('#question-question, #question-expected, #question-active').attr("readonly", false);
  }

  function resetForm() {
    // Render input fields uneditable again
    $('#question-question, #question-expected, #question-active').attr("readonly", true);

    // Reset the form to original state
    // $("#edit-question-form").trigger('reset');
  }

  function showButtons() {
    $('#btn-cancel').attr('hidden', true)
    $('#btn-delete').attr('hidden', false)
    $('#btn-update').attr('hidden', false)
  }

  function hideButtons() {
    $('#btn-cancel').attr('hidden', false)
    $('#btn-delete').attr('hidden', true)
    $('#btn-update').attr('hidden', true)
  }

  function handler1() {
    editForm()
    showButtons()
    $('#collapseInfo').collapse('show')
    $(this).one("click", handler2)
  }

  function handler2() {
    resetForm()
    hideButtons()
    $('#collapseInfo').collapse('hide')
    $(this).one("click", handler1)
  }

  $("#question-edit").one("click", handler1);

</script>
@stop

{{-- @extends('main')

@section('title', '| Questions')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <div class="d-flex justify-content-between flex-wrap">
      <div class="d-flex align-items-center">
        <h2 class="mr-2">Medical Questions</h2>
      </div>
      <div class="d-flex align-items-center">
        <a href="{{ route('questions.create') }}" class="btn btn-primary btn-sm">New</a>
      </div>
    </div>
    <div class="d-flex justify-content-between flex-wrap mb-3">
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-primary">{{ $active }} Active
        </span>
      </div>
      <div class="d-flex align-items-center">
        <span class="badge badge-pill badge-secondary"> {{ $inactive }} Inactive</span>
      </div>
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Question</th>
            <th scope="col">Expected Answer</th>
            <th class="btn-dashboard"></th>
          </tr>
        </thead>
        <tbody>

          @foreach($questions as $question)
          <tr>
            <th>{{ $question->id }}</th>
            <td>{{ $question->question }}</td>
            <td>{{ $question->expected == "1" ? "Yes" : "No" }}</td>
            <td class="btn-toolbar-fix">
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('questions.show', $question->id) }}" class="btn btn-outline-dark btn-xs mr-2">View</a>
                <a href="{{ route('questions.edit', $question->id) }}" class="btn btn-outline-dark btn-xs">Edit</a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

@stop --}}