@extends('main')

@section('title', '| Show Question')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <h2>Show Question</h2>
    <div class="card mb-2">
      <div class="card-body">
        <p class="card-text">{{ $question->question }}</p>
        <p class="text-muted"><small>Author: Nicholas James</small></p>
        <p class="card-text">Expected Answer: {{ $question->expected == "1" ? "Yes" : "No" }}</p>
        <p class="card-text">Active: {{ $question->expected == "1" ? "True" : "False" }}</p>
      </div>
      <div class="card-footer text-muted d-flex justify-content-between">
        <small>Created: {{ date('M j, Y H:i', strtotime($question->created_at)) }}</small>
        <small>Updated: {{ date('M j, Y H:i', strtotime($question->updated_at)) }}</small>
      </div>
    </div>
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
      <div class="btn-group col mr-2" role="group" aria-label="Cancel">
        <a href="{{ route('questions.index') }}" type="button" class="btn btn-block btn-light">Back</a>
      </div>
      <div class="btn-group col" role="group" aria-label="Edit">
        @if(Auth::user()->role_id < 3)
        <a href="{{ route('questions.edit', $question->id) }}" type="button" class="btn btn-block btn-primary">Edit</a>
        @endif
      </div>
    </div>
  </div>
</div>

@stop