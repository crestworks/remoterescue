@extends('main')

@section('title', '| Create Question')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <h2>Create Medical Question</h2>

    <form action="{{ url('questions') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="question">Question</label>
        <textarea type="text" class="form-control" name="question" aria-describedby="question" placeholder="Question goes here... "></textarea>
        <small>Remember to end it with a question mark (?) and that it must be a yes/no question</small>
      </div>
      <div class="form-group">
        <label for="expected">Expected Answer</label>
        <select class="form-control" name="expected">
          <option>Select</option>
          <option value="1">YES</option>
          <option value="0">NO</option>
        </select>
      </div>
      <div class="form-group">
        <label for="active">Active</label>
        <select class="form-control" name="active">
          <option>Select</option>
          <option value="1">True</option>
          <option value="0">False</option>
        </select>
      </div>
      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group col mr-2" role="group" aria-label="Cancel">
          <a href="{{ route('packages.index') }}" type="button" class="btn btn-block btn-light">Cancel</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Submit">
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
      </div>
    </form>

  </div>
</div>

@stop