@extends('main')

@section('title', '| Edit Question')

@section('content')

<div class="row pb-80">
  <div class="col-md-8">
    <h2>Edit Question</h2>

    
    <form action="{{ url('questions', $question->id) }}" method="POST">

      {{ method_field('PUT') }}
      {{ csrf_field() }}

      <div class="form-group">
        <textarea type="text" class="form-control" name="question" aria-describedby="question" placeholder="Question goes here... ">{{ $question->question }}</textarea>
        <small>By updating this Question you are also updating the name associated with the question.</small>
        <small>Remember to end it with a question mark (?) and that it must be a yes/no question.</small>
      </div>
      <div class="form-group">
        <label for="expected">Expected Answer</label>
        <select class="form-control" name="expected">
          <option>Select</option>
          <option value="1" {{ $question->active == "1" ? 'selected' : '' }}>Yes</option>
          <option value="0" {{ $question->active == "0" ? 'selected' : '' }}>No</option>
        </select>
      </div>
      <div class="form-group">
        <label for="active">Active</label>
        <select class="form-control" name="active">
          <option>Select</option>
          <option value="1" {{ $question->active == "1" ? 'selected' : '' }}>True</option>
          <option value="0" {{ $question->active == "0" ? 'selected' : '' }}>False</option>
        </select>
      </div>
      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group col mr-2" role="group" aria-label="Back">
          <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light">Back</a>
        </div>
        <div class="btn-group col" role="group" aria-label="Update">
          <button type="submit" class="btn btn-block btn-primary">Update</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-4">
    <div class="card mb-2">
      <div class="card-body">
        <h5 class="card-title">Original Question</h5>
        <p class="card-text">{{ $question->question }}</p>
        <p class="card-text">Expected Answer: {{ $question->expected == "1" ? "Yes" : "No" }}</p>
        <p class="card-text">Active: {{ $question->active == "1" ? "True" : "False" }}</p>
        <p class="comment-name">- Nicholas James</p>
      </div>
      <div class="card-footer text-muted d-flex justify-content-between">
        <small>Created: {{ date('M j, Y H:i', strtotime($question->created_at)) }}</small>
        <small>Updated: {{ date('M j, Y H:i', strtotime($question->updated_at)) }}</small>
      </div>
    </div>
  </div>
</div>

@stop