@extends('main')

@section('title', '| Create Subscriber')

@section('content')

<div class="row pb-80">
  <div class="col-md-6 offset-md-3">
    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Create Subscriber</h3>
      <a href="{{ route('individual') }}" class="btn-icon-primary lead"><i class="fas fa-times"></i></a>
    </div>
    <hr>
    <form action="{{ url('subscribers') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="last">Last Name</label>
        <input type="text" class="form-control" name="last" aria-describedby="last" placeholder="Enter last name">
      </div>
      <div class="form-group">
        <label for="first">First Name</label>
        <input type="text" class="form-control" name="first" placeholder="Enter fist name">
      </div>
      <div class="form-group">
        <label for="dob">Date of Birth</label>
        <input type="date" class="form-control" name="dob" placeholder="MM/DD/YYYY">
      </div>
      <div class="form-group">
        <label for="idNumber">National ID/Passport Number</label>
        <input type="text" class="form-control" name="idNumber" placeholder="Enter ID number with no spaces or dashes">
      </div>
      <div class="form-group">
        <label for="mobile">Mobile Number</label>
        <input type="tel" class="form-control" name="mobile" placeholder="+263771234567">
        <small>Make sure to include the country code and drop the first the 0 of the mobile number. Do not add anny unnecessary characters.</small>
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" placeholder="example@subscriber.com">
      </div>
      <div class="form-group">
        <label for="weight">Weight (kg)</label>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="78" aria-label="weight" name="weight" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">kg</span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="package_id">Package</label>
        <select class="form-control" name="package_id">
          <option>Select</option>
          @foreach ($packages as $package)
          <option value="{{ $package->id }}">{{ $package->title }}</option>
          @endforeach
        </select>
      </div>

      <h3>Medical History</h3>
      <div class="card bg-light mb-3">
        <div class="card-body">
          @foreach($questions as $question)
          <div class="form-group">
            <label for="question_{{ $question->id }}">{{ $question->question}}</label>
            <select class="form-control" name="answers[{{ $question->id }}]">
              <option value="S">Select</option>
              <option value="1">Yes</option>
              <option value="0">No</option>
            </select>
          </div>
          @endforeach
        </div>
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-block btn-outline-primary">Submit</button>
      </div>
    </form>
  </div>
</div>

@stop