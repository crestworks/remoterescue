@extends('main')

@section('title', '| Edit Subscriber')

@section('content')

<div class="row pb-80">
  <div class="col-md-6 offset-md-3">
    <div class="d-flex justify-content-between align-items-baseline">
      <h3>Edit Subscriber</h3>
      <a href="{{ url()->previous() }}" class="btn-icon-primary lead"><i class="fas fa-arrow-left"></i></a>
    </div>
    <hr>

    <form action="{{ url('subscribers', $subscriber->id) }}" method="POST">

      {{ method_field('PUT') }}
      {{ csrf_field() }}

      <div class="form-group">
        <label for="accountNumber">Account Number</label>
        <input readonly type="text" class="form-control" id="accountNumber" aria-describedby="accountNumber" value="{{ Helper::rr_subscriber_id( $subscriber->id) }}">
      </div>
      <div class="form-group">
        <label for="last">Last Name</label>
        <input type="text" class="form-control" name="last" aria-describedby="last" value="{{ $subscriber->last }}">
      </div>
      <div class="form-group">
        <label for="first">First Name</label>
        <input type="text" class="form-control" name="first" value="{{ $subscriber->first }}">
      </div>
      <div class="form-group">
        <label for="dob">Date of Birth</label>
        <input type="date" class="form-control" name="dob" value="{{ $subscriber->dob }}">
      </div>
      <div class="form-group">
        <label for="idNumber">National ID/Passport Number</label>
        <input type="text" class="form-control" name="idNumber" value="{{ $subscriber->idNumber }}">
      </div>
      <div class="form-group">
        <label for="mobile">Mobile Number</label>
        <input type="tel" class="form-control" name="mobile" value="{{ $subscriber->mobile }}">
        <small>Make sure to include the country code and drop the first the 0 of the mobile number. Do not add any unnecessary characters.</small>
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" value="{{ $subscriber->email }}">
      </div>
      <div class="form-group">
        <label for="weight">Weight (kg)</label>
        <div class="input-group mb-3">
          <input type="text" class="form-control" value="{{ $subscriber->weight }}" aria-label="weight" name="weight" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">kg</span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="package_id">Package</label>
        <select class="form-control" name="package_id">
          <option>Select</option>
          @foreach ($packages as $package)
          <option {{ $subscriber->package_id == $package->id ? 'selected' : '' }} value="{{ $package->id }}">{{ $package->title }}</option>
          @endforeach
        </select>
      </div>

      <h3>Medical History</h3>

      @foreach($subscriber->questions as $question)
      <div class="form-group">
        <label for="question_{{ $question->id }}">{{ $question->question}}</label>
        <select class="form-control" name="answers[{{ $question->id }}]">
          <option value="S" {{ $question->pivot->answer == 'S' ? 'selected' : '' }}>Select</option>
          <option value="1" {{ $question->pivot->answer == '1' ? 'selected' : '' }}>Yes</option>
          <option value="0" {{ $question->pivot->answer == '0' ? 'selected' : '' }}>No</option>
        </select>
      </div>
      @endforeach

      @foreach($un_answered as $question)
      <div class="form-group">
        <label for="question_{{ $question->id }}">{{ $question->question}}</label>
        <select class="form-control" name="answers[{{ $question->id }}]">
          <option value="S">Select</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      @endforeach

      <h3>Vetting</h3>

      <div class="form-group">
        <label for="vetting">Status</label>
        <select class="form-control" name="vetting">
          <option>Select</option>
          <option value="1" {{ $subscriber->vetting > '0' ? 'selected' : '' }}>Pass</option>
          <option value="0" {{ $subscriber->vetting == '0' ? 'selected' : '' }}>Fail</option>
          <option value="-1" {{ $subscriber->vetting < '0' ? 'selected' : '' }}>Processing</option>
        </select>
      </div>

      <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group col p-0 mr-4" role="group" aria-label="Cancel">
          <a href="{{ url()->previous() }}" type="button" class="btn btn-block btn-light">Back</a>
        </div>
        <div class="btn-group col p-0" role="group" aria-label="Submit">
          <button type="submit" class="btn btn-block btn-primary">Update</button>
        </div>
      </div>
    </form>

  </div>
</div>

@stop