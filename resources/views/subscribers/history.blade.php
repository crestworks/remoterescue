@extends('main')

@section('title', '| Questions')

@section('content')

<div class="row pb-80">
  <div class="col-md-8 offset-md-2">
    <div class="d-flex justify-content-between flex-wrap">
      <div class="d-flex align-items-center">
        <h2 class="mr-2">Medical History - {{ $subscriber->first }}</h2>
      </div>
    </div>
    <div class="table-responsive-md">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Question</th>
            {{-- <th scope="col">Expected Answer</th> --}}
            <th>Answer</th>
          </tr>
        </thead>
        <tbody>

          @foreach($questions as $question)
          <tr>
            <th scope="row">{{ $question->id }}</th>
            <td>{{ $question->question }}</td>
            {{-- <td>{{ $question->expected == "1" ? "Yes" : "No" }}</td> --}}
            <td class="btn-toolbar-fix">
              <div class="btn-toolbar justify-content-end">
                <a href="{{ route('questions.show', $question->id) }}" class="btn btn-outline-dark btn-xs mr-2">Yes</a>
                <a href="{{ route('questions.edit', $question->id) }}" class="btn btn-outline-dark btn-xs">Save</a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

@stop