<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function subscriber()
    {
    	return $this->belongsToMany('App\Subscriber')->withTimeStamps();
    }

    public function family()
    {
    	return $this->belongsToMany('App\Family')->withTimeStamps();
    }

    public function corporate()
    {
    	return $this->belongsToMany('App\Corporate')->withTimeStamps();
    }

    public function safari()
    {
    	return $this->belongsToMany('App\Safari')->withTimeStamps();
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    } 
}
