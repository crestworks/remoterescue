<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function group_type()
    {
        return $this->belongsTo('App\Group_type');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber')->withTimeStamps();
    }

    public function comment_groups()
    {
    	return $this->hasMany('App\Comment_group');
    }
}
