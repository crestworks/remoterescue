<?php

namespace App\Helpers;

class Helper

/**
 * Helper functions are here to help preserve the DRY code philosophy
 * the "rr_" is in front of each function to designate that it is a Helper and that they are specific to the Remote Rescue program
 * the rest of the function name will help describe what the fuction does
 */

{

	/**
	 * The following is used to generate the tagging system
	 */
	public static function rr_tags($start_date, $end_date)
	{
		if((new \DateTime(date('Y-m-d', strtotime($start_date))))->diff(new \DateTime(date('Y-m-d', strtotime($end_date))))->format('%m') == 1) {
			return __('text-dark');
		} elseif ((round(strtotime($end_date) - time()) / (60 * 60 * 24)) > 30) {
			return __('text-primary');
		} elseif((round(strtotime($end_date) - time()) / (60 * 60 * 24)) < 0) {
			return __('text-danger');
		} elseif((new \DateTime(date('Y-m-d', strtotime($end_date))))->diff(new \DateTime(date('Y-m-d', strtotime(time()))))->format('%d') < 31) {
			return __('text-warning');
		} else {
			return __('');
		}
	}

	/**
	 * This helper function is used to clean up the code on subscriber.show page
	 * Creates a link to the group the subscriber belongs to
	 * We are assuming that the subscriber belongs to a group
	 */
	public static function rr_package_link($package_id)
	{
		if($package_id == 6) {
			return 'family';
		} elseif ($package_id == 7) {
			return 'corporate';
		} elseif ($package_id == 8) {
			return 'safari';
		}

		return 'individual';
	}

	/**
	 * The follow is all the variables containing payment values
	 * If there are changes to costs thes variables must be edited
	 * Later these changes will be dynamic but for now they are fixed
	 * Package Rates are as follows:
	 * - Vetting duration = 14 days
	 * - Joining Fee = $15 (except for corporate)
	 * - Individual Subscription 
	 *      = $18/mo (minimum payment is for 6 months)
	 *      = $180/yr
	 *      = $270/yr (over 70 years old)
	 * - Family Subscription 
	 *      = $360/yr (up to 4 members)
	 *      = $480/yr (if there is any member of the group over 70 yrs)
	 *      = $90/yr (for each additional member over the 4)
	 * - Regional Subscription 
	 *      = $50/mo (minimum payment is for 6 months)
	 *      = $480/yr
	 *      = $810/yr (over 70 years old)
	 * - Regional Ex Subscription 
	 *      = $110/mo (minimum payment is for 6 months)
	 *      = $1060/yr
	 *      = $1890/yr (over 70 years old)
	 * - Corporate Subscription (10 - 50 staff) 
	 *      = $10/p Joining fee 
	 *      = $12.50/p/mo (minimum payment is for 6 months)
	 *      = additional 50% for over 70 years
	 * - Safari Subscription 
	 *      = $18/mo (minimum payment is for 6 months)
	 *      = $180/yr
	 *      = $270/yr (over 70 years old)
	 * - Daily Subscription 
	 *      = $15/day
	 */
	public static function rr_last_payment($past_payments, $payment_id)
	{
		$last_payment = [];

		for($x = 0; $x < $past_payments->count() - 1; $x++) {
			if($past_payments[$x]->id == $payment_id) {
				array_push($last_payment, $past_payments[$x + 1]);
			}
		}
		return $last_payment;
	}

	public static function rr_end_date($amount, $uri, $maker, $last_payment, $dependants, $beds)
	{
		// This function will help calculate the end_date of a payment period
		if(strpos($uri, 'subscriber') !== false || $maker == 'subscriber') {
			/**
			 * - Joining Fee = $15 (except for corporate)
			 * - Individual Subscription (package_id = 1)
			 *      = $18/mo (minimum payment is for 6 months)
			 *      = $180/yr
			 *      = $270/yr (over 70 years old)
			 * - Regional Subscription (package_id = 3)
			 *      = $50/mo (minimum payment is for 6 months)
			 *      = $480/yr
			 *      = $810/yr (over 70 years old)
			 * - Regional + Subscription (package_id = 4)
			 *      = $110/mo (minimum payment is for 6 months)
			 *      = $1060/yr
			 *      = $1890/yr (over 70 years old)
			 * - Daily Subscription (package_id = 5)
     		 *      = $15/day
			 */
			$joining_fee = 15;
			$member_mo = 0; // per month
			$member_yr = 0; // per year

			// dd($dependants);
			$over_70_date = strtotime('-70 years', strtotime(date('m-d-y'))); // what is the dob someone would have to be to be over 70yrs

			if($dependants[0]->package_id == 1) {
				$member_mo = 18; // per month
				$member_yr = 180; // per year

				if($dependants[0]->dob < $over_70_date) {
					$member_yr = 270; // per year
				}
			} elseif($dependants[0]->package_id == 3) {
				$member_mo = 50; // per month
				$member_yr = 480; // per year

				if($dependants[0]->dob < $over_70_date) {
					$member_yr = 810; // per year
				}
			} elseif($dependants[0]->package_id == 4) {
				$member_mo = 110; // per month
				$member_yr = 1060; // per year

				if($dependants[0]->dob < $over_70_date) {
					$member_yr = 1890; // per year
				}
			} elseif($dependants[0]->package_id == 5) {
				$daily = 15;
				if(count($last_payment) == 0) { // first time payments include joining fees
					// dd($member_yr + $joining_fee);
					if($amount >= $daily + $joining_fee) {
						// dd('0 / daily / exact');
						return '+1 day'; // duration in days
					} else {
						// dd('0 / daily / exact');
						return '+0 day'; // duration in days
					}
				} else {
					// dd($member_yr + $joining_fee);
					if($amount >= $daily) {
						// dd('0 / daily / exact');
						return '+1 day'; // duration in days
					} else {
						// dd('0 / daily / exact');
						return '+0 day'; // duration in days
					}
				}
			}
			// dd($last_payment);

			if(count($last_payment) == 0) { // first time payments include joining fees
				// dd($member_yr + $joining_fee);
				if($amount == $member_yr + $joining_fee) {
					// dd('0 / yearly / exact');
					return '+12 months'; // duration in months
				} else {
					$duration = round(($amount - $joining_fee) / $member_mo);
					$expected_amount = ($duration * $member_mo) + $joining_fee;
				
					// dd($expected_amount);
					// dd($duration);
					if($amount == $expected_amount) {
						// dd('0 / monthly / exact');
						return '+' . $duration . ' months'; // duration in months
					} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1 && $duration > 5) {
						// dd('0 / monthly / 10%');
						return '+1 month'; // duration in months
					} else {
						// dd('0 / monthly / >10%');
						return '+1 day'; // duration in days
					}
				}
			} else {
				if($amount == $member_yr) {
					// dd('+1 / yearly / exact');
					return '+12 months'; // duration in months
				} else {
					$duration = round($amount / $member_mo);
					$expected_amount = $duration * $member_mo;
				
					// dd($expected_amount);
					if($amount == $expected_amount) {
						// dd('+1 / monthly / exact');
						return '+' . $duration . ' months'; // duration in months
					} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
						// dd('+1 / monthly / 10%');
						return '+1 month'; // duration in months
					} else {
						// dd('+1 / monthly / >10%');
						return '+1 day'; // duration in days
					}
				}
			}
		}

		// This function will help calculate the end_date of a payment period
		if(strpos($uri, 'family') !== false || $maker == 'family') {
			/**
			 * - Family Subscription 
			 *      = $360/yr (up to 4 members)
			 *      = $480/yr (if there is any member of the group over 70 yrs)
			 *      = $90/yr (for each additional member over the 4)
			 */
			$joining_fee = 15;
			$young_members = 360;
			$old_members = 480;
			$more_members = 90;

			$dependants_count = $dependants->count();

			// are there now any dependants over the age of 70yrs
			$over_70 = 0; // how many members are now over 70yrs
			$over_70_date = strtotime('-70 years', strtotime(strtotime('+1 month', strtotime(time())))); // what is the dob someone would have to be to be over 70yrs within the next month
			foreach($dependants as $dependant) { // check to see if any dependants are over 70yrs
				if($dependant->dob > $over_70_date) {
					$over_70 = $over_70++;
				}
			}

			if(count($last_payment) == 0) { // first time payments include joining fees
				if($dependants_count < 5) {
					$expected_amount = $over_70 == 0 ? $young_members + ($joining_fee * $dependants_count) : $old_members + ($joining_fee * $dependants_count);
					// dd($expected_amount);
					if($amount == $expected_amount) {
						// dd('0 / 4 / exact');
						return '+12 months'; // duration in months
					} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
						// dd('0 / 4 / 10%');
						return '+1 month'; // duration in months
					} else {
						// dd('0 / 4 / >10%');
						return '+1 day'; // duration in days
					}
				} else {
					$expected_amount = $over_70 == 0 ? $young_members + ($joining_fee * $dependants_count) + (($dependants_count - 4) * $more_members) : $old_members + ($joining_fee * $dependants_count) + (($dependants_count - 4) * $more_members);
					
					if($amount == $expected_amount) {
						// dd('0 / +4 / exact');
						return '+12 months'; // duration in months
					} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
						// dd('0 / +4 / 10%');
						return '+1 month'; // duration in months
					} else {
						// dd('0 / +4 / >10%');
						return '+1 day'; // duration in days
					}
				}
			} else { // more than 0 payments
				$last_payment_dependants = $last_payment[0]->dependants;
				$dependants_difference =  $dependants_count - $last_payment_dependants; // check to see if the number of dependants has increased or decreased

				// dd('more than 0 payments');
				// dd($dependants_difference);
				if($dependants_count < 5) {
					if($last_payment_dependants < $dependants_count) { // number of dependants has increased
						$expected_amount = $over_70 == 0 ? $young_members + ($joining_fee * $dependants_difference) : $old_members + ($joining_fee * $dependants_difference);
						if($amount == $expected_amount) {
							// dd('+1 / 4 / exact');
							return '+12 months'; // duration in months
						} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
							// dd('+1 / 4 / 10%');
							return '+1 month'; // duration in months
						} else {
							// dd('+1 / 4 / >10%');
							return '+1 day'; // duration in days
						}
					} else { // number of dependants has decreased
						$expected_amount = $over_70 == 0 ? $young_members : $old_members;
						if($amount == $expected_amount) {
							// dd('+1 / +4 / exact');
							return '+12 months'; // duration in months
						} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
							// dd('+1 / +4 / 10%');
							return '+1 month'; // duration in months
						} else {
							// dd('+1 / +4 / >10%');
							return '+1 day'; // duration in days
						}
					}
				}
			}
		} elseif(strpos($uri, 'corporate') !== false || $maker == 'corporate') {
			/**
			 * - Corporate Subscription (10 - 50 staff) 
			 *      = $10/p Joining fee 
			 *      = $12.50/p/mo (minimum payment is for 6 months)
			 *      = additional 50% for over 70 years
			 */
			$old_multiplier = 1.5;
			$joining_fee = 10; // per person
			$joining_fee_old = $joining_fee * $old_multiplier;
			$young_members = 12.50; // per person per month
			$old_members = $young_members * $old_multiplier; // per person per month

			$dependants_count = $dependants->count();

			if($dependants_count > 50) {
				$young_members = 7.5; // per person per month
				$old_members = $young_members * $old_multiplier; // per person per month
			}

			// are there now any dependants over the age of 70yrs
			$over_70 = 0; // how many members are now over 70yrs
			$over_70_date = strtotime('-70 years', strtotime(time())); // what is the dob someone would have to be to be over 70yrs at date of payment
			foreach($dependants as $dependant) { // check to see if any dependants are over 70yrs
				if($dependant->dob > $over_70_date) {
					$over_70 = $over_70++;
				}
			}
			// dd($last_payment);
			// dd(count($last_payment));

			if(count($last_payment) == 0) { // first time payments include joining fees
				if($over_70 == 0) {
					$duration = round((($amount - ($dependants_count * $joining_fee)) / $dependants_count / $young_members), 0);
					$expected_amount = round((($dependants_count * $young_members * $duration) + ($dependants_count * $joining_fee)), 0);
					// dd($duration);
					// dd($expected_amount);
					if($amount == $expected_amount && $duration > 5) {
						// dd('exact');
						return '+' . $duration . ' months'; // duration in months
					} elseif($amount > $expected_amount * .95 && $amount < $expected_amount * 1.05 && $duration > 5) {
						// dd('5% error');
						return '+1 month'; // duration in months
					} else {
						// dd('> 5% error');
						return '+1 day'; // duration in days
					}
				} else {
					$duration = floor($amount / ((($dependants_count - $over_70) * ($young_members + $joining_fee)) + (($over_70) * ($old_members + $joining_fee_old))));
					$expected_amount = floor($duration * ((($dependants_count - $over_70) * ($young_members + $joining_fee)) + ($over_70 * ($young_members + $joining_fee))));

					if($amount == $expected_amount && $duration > 5) {
						return '+' . $duration . ' months'; // duration in months
					} elseif($amount > $expected_amount * .95 && $amount < $expected_amount * 1.05 && $duration > 5) {
						return '+1 month'; // duration in months
					} else {
						return '+1 day'; // duration in days
					}
				}
			} else { // more than 0 payments
				$last_payment_dependants = $last_payment[0]->dependants;
				$dependants_difference =  $dependants_count - $last_payment_dependants; // check to see if the number of dependants has increased or decreased
				// dd($last_payment_dependants);
				// dd($dependants_difference);
				if($last_payment_dependants < $dependants_count) { // number of dependants has increased
					
					$dep_diff_over_70 = 0;

					for($x = $last_payment_dependants; $x < $dependants_count; $x++) {
						if($dependants[$x]->dob > $over_70_date) {
							$dep_diff_over_70 = $dep_diff_over_70++;
						}
					}

					if($dep_diff_over_70 == 0) {
						$duration = floor(($amount - ($dependants_difference * $joining_fee)) / ((($dependants_count - $over_70) * $young_members) + ($over_70 * $old_members)));
						$expected_amount = floor(($duration * ((($dependants_count - $over_70) * $young_members) + ($over_70 * $young_members))) + ($dependants_difference * $joining_fee));

						if($amount == $expected_amount && $duration > 5) {
							return '+' . $duration . ' months'; // duration in months
						} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1 && $duration > 5) {
							return '+1 month'; // duration in months
						} else {
							return '+1 day'; // duration in days
						}
					} else {
						$duration = floor(($amount - ((($dependants_difference - $dep_diff_over_70) * $joining_fee) + ($dep_diff_over_70 * $joining_fee_old)) / ((($dependants_count - $over_70) * $young_members) + ($over_70 * $old_members))));
						$expected_amount = floor(($duration * ((($dependants_count - $over_70) * $young_members) + ($over_70 * $young_members))) + (($dependants_difference * $joining_fee) + ($dep_diff_over_70 * $joining_fee_old)));

						if($amount == $expected_amount && $duration > 5) {
							return '+' . $duration . ' months'; // duration in months
						} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1 && $duration > 5) {
							return '+1 month'; // duration in months
						} else {
							return '+1 day'; // duration in days
						}
					}
				} else {
					$duration = round($amount / ((($dependants_count - $over_70) * $young_members) + ($over_70 * $old_members)));
					$expected_amount = round($duration * ((($dependants_count - $over_70) * $young_members) + ($over_70 * $young_members)));
					// dd($duration);
					// dd($expected_amount);
					if($amount == $expected_amount && $duration > 5) {
						// dd('exact');
						return '+' . $duration . ' months'; // duration in months
					} elseif($amount > $expected_amount * .95 && $amount < $expected_amount * 1.05 && $duration > 5) {
						// dd('5% error');
						return '+1 month'; // duration in months
					} else {
						// dd('>5% error');
						return '+1 day'; // duration in days
					}
				}
			}
		} elseif(strpos($uri, 'safari') !== false || $maker == 'safari') {
			/**
			 * - Safari Subscription 
			 * 		= $30 Joining fee
			 *      = $100/bed/yr
			 *      = $50/member/yr
			 */
			$joining_fee = 30; // per camp
			$member = 50; // per person per year
			$bed = 100; // per bed per year

			$dependants_count = $dependants->count();

			// dd(count($last_payment));
			if(count($last_payment) == 0) { // first time payments include joining fees
				$expected_amount = $joining_fee + ($dependants_count * $member) + ($beds * $bed);
				if($amount == $expected_amount) {
					// dd('exact');
					return '+12 months'; // duration in months
				} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
					// dd('10%');
					return '+1 month'; // duration in months
				} else {
					// dd('>10%');
					return '+1 day'; // duration in days
				}
			} else {
				$expected_amount = ($dependants_count * $member) + ($beds * $bed);
				if($amount == $expected_amount) {
					// dd('exact');
					return '+12 months'; // duration in months
				} elseif($amount > $expected_amount * .9 && $amount < $expected_amount * 1.1) {
					// dd('10%');
					return '+1 month'; // duration in months
				} else {
					// dd('>10%');
					return '+1 day'; // duration in days
				}
			}
		}
	}

	public static function rr_payment_individual($id, $start_date, $amount)
	{
		// This function will help process the payment is the Payments Controller
	}

	public static function rr_payment_regional($id, $start_date, $amount)
	{
		// This function will help process the payment is the Payments Controller
	}

	public static function rr_payment_regionalex($id, $start_date, $amount)
	{
		// This function will help process the payment is the Payments Controller
	}

	public static function rr_payment_daily($id, $start_date, $amount)
	{
		// This function will help process the payment is the Payments Controller
	}

	public static function rr_payment_due($package_id, $start_date, $amount)
	{
		//return strtoupper($string);

		/**
		 * Receiving information
		 * package_id
		 * start_date
		 * amount
		 * age: still need to factor age into this function
		 */

		if  ($package_id == 1) {
			if($amount == 108 || $amount == 180) {
				if ($amount == 108) {
					return date('M j, Y', strtotime('+6 month', strtotime($start_date)));
				} else {
					return date('M j, Y', strtotime('+12 month', strtotime($start_date)));
				}
			}
		}	elseif ($package_id == 3) {
			if($amount == 300 || $amount == 480) {
				if ($amount == 300) {
					return date('M j, Y', strtotime('+6 month', strtotime($start_date)));
				} else {
					return date('M j, Y', strtotime('+12 month', strtotime($start_date)));
				}
			}
		} elseif ($package_id == 4) {
			if($amount == 660 || $amount == 1060) {
				if ($amount == 660) {
					return date('M j, Y', strtotime('+6 month', strtotime($start_date)));
				} else {
					return date('M j, Y', strtotime('+12 month', strtotime($start_date)));
				}
			}
		} 
  }

	public static function rr_payment_display($package_id, $start_date, $amount)
	{
    	//return strtoupper($string);

		/**
		 * Receiving information
		 * package_id
		 * start_date
		 * amount
		 * age: still need to factor age into this function
		 */

		if ($package_id == 1) {
			if ($amount == 108 || $amount == 180) {
				if ($amount == 108) {   
					$due_date = new \DateTime(date('Y-m-d', strtotime('+6 month', strtotime($start_date))));
					$now = new \DateTime();

					if ($due_date < $now) {
						return 'bg-danger text-light';
					} elseif ($due_date->diff($now)->format('%a') <= '30') {
						return 'bg-warning';
					}
					return '';
				} else {
					$due_date = new \DateTime(date('Y-m-d', strtotime('+12 month', strtotime($start_date))));
					$now = new \DateTime();

					if ($due_date < $now) {
						return 'bg-danger text-light';
					} elseif ($due_date->diff($now)->format('%a') <= '30') {
						return 'bg-warning';
					}
					return '';
				}
			}
		}	elseif ($package_id == 3) {
			if($amount == 300 || $amount == 480) {
				if ($amount == 300) {
					$due_date = new \DateTime(date('Y-m-d', strtotime('+6 month', strtotime($start_date))));
					$now = new \DateTime();

					if ($due_date < $now) {
						return 'bg-danger text-light';
					} elseif ($due_date->diff($now)->format('%a') <= '30') {
						return 'bg-warning';
					}
					return '';
				} else {
					$due_date = new \DateTime(date('Y-m-d', strtotime('+12 month', strtotime($start_date))));
					$now = new \DateTime();

					if ($due_date < $now) {
						return 'bg-danger text-light';
					} elseif ($due_date->diff($now)->format('%a') <= '30') {
						return 'bg-warning';
					}
					return '';
				}
			}
		} elseif ($package_id == 4) {
			if($amount == 660 || $amount == 1060) {
				$due_date = new \DateTime(date('Y-m-d', strtotime('+6 month', strtotime($start_date))));
				$now = new \DateTime();

				if ($due_date < $now) {
					return 'bg-danger text-light';
				} elseif ($due_date->diff($now)->format('%a') <= '30') {
					return 'bg-warning';
				}
				return '';
			} else {
				$due_date = new \DateTime(date('Y-m-d', strtotime('+12 month', strtotime($start_date))));
				$now = new \DateTime();

				if ($due_date < $now) {
					return 'bg-danger text-light';
				} elseif ($due_date->diff($now)->format('%a') <= '30') {
					return 'bg-warning';
				}
				return '';
			}
		}
		return 'bg-secondary text-light';
	}

	public static function rr_credit_display(string $string)
	{
		/**
		 * Receiving information
		 * package_id
		 * count: how many payments have been made
		 * amount
		 */

		// If package_id == 1 (Individual), amount should >= 108 || amount >= ___
			// If the amount >= 108 then the package is valid for 6 months
				// If the amount is > 108 then this must be indicated with a bg-success
			// elseif amount >= ___ then the package is valid for 12 months
				// If the amount is > ___ then this must be indicated with a bg-success
  }

  /*
   * The rr_subscriber_id function takes the database row id and converts it
   * The ID that is specific to the Subscriber
   * And decribes that their Invividual ID #
   */

  public static function rr_subscriber_id($id)
  {
  	// creating unique 6 digit account number, where first digit is based on package, and the remaining 5 digits are based off id
    $accountNumber_len = 5;
    $zeros = '';

    $diff_len  = $accountNumber_len - strlen($id);

    for($x = 0; $x < $diff_len; $x++) {
        $zeros = $zeros . '0';
    }

    return 'I' . $zeros . $id;
  }

  /*
   * The rr_group_id function takes the database row id and converts it
   * The ID that is specific to the Group
   * And decribes that their Group ID #
   */

  public static function rr_group_id($id)
  {
  	// creating unique 6 digit account number, where first digit is based on package, and the remaining 5 digits are based off id
    $accountNumber_len = 5;
    $zeros = '';

    $diff_len  = $accountNumber_len - strlen($id);

    for($x = 0; $x < $diff_len; $x++) {
        $zeros = $zeros . '0';
    }

    return 'G' . $zeros . $id;
  }
}