<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber', 'question_subscriber')->withPivot('answer')->withTimeStamps();
    }
}
