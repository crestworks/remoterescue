<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group')->withTimeStamps();
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'question_subscriber')->withPivot('answer')->withTimeStamps();
    }

    public function comments()
    {
    	return $this->belongsToMany('App\Comment');
    }

    public function payments()
    {
        return $this->belongsToMany('App\Payment')->orderBy('paid_date', 'desc');;
    }
}
