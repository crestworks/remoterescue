<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment_group extends Model
{
    public function group()
    {
    	return $this->belongsTo('App\Group');
    }
}
