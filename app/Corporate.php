<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{
    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber')->withTimeStamps();
    }

    public function comments()
    {
    	return $this->belongsToMany('App\Comment');
    }

    public function payments()
    {
        return $this->belongsToMany('App\Payment')->orderBy('paid_date', 'desc');;
    }
}
