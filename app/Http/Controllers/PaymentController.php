<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Payment;
use App\Subscriber;
use App\Family;
use App\Corporate;
use App\Safari;
use App\Helpers\Helper;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $package_description)
    {
        $uri = Helper::package_descrtion($package_description);
        $payments = collect();
        $entity = collect();
        $id = $id;
        $maker = 'maker';
        $makers = 'makers';

        if(strpos($uri, 'subscriber') !== false) {
            $entity = Subscriber::find($id);
            $payments = $entity->payments;
            $maker = 'subscriber';
            $makers = 'subscribers';
        } elseif(strpos($uri, 'family') !== false) {
            $entity = Family::find($id);
            $payments = $entity->payments;
            $maker = 'family';
            $makers = 'families';
        } elseif(strpos($uri, 'corporate') !== false) {
            $entity = Corporate::find($id);
            $payments = $entity->payments;
            $maker = 'corporate';
            $makers = 'corporates';
        } elseif(strpos($uri, 'safari') !== false) {
            $entity = Safari::find($id);
            $payments = $entity->payments;
            $maker = 'safari';
            $makers = 'safaris';
        }

        return view('payments.index')
            ->withEntity($entity)
            ->withPayments($payments)
            ->withId($id)
            ->withMaker($maker)
            ->withMakers($makers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // This is the url that originated saving the payment
        $uri = url()->previous();
        $maker = '';
        $dependants = collect();
        $past_payments = [];
        $last_payment = [];
        $beds = 0; // only for safari camps

        // validate data
        $this->validate($request, array(
            'amount' => 'required|digits_between:2,8',
            'paid_date' => 'required|date'
        ));

        // store in the database
        $payment = new Payment;

        $payment->amount = $request->amount;
        $payment->paid_date = $request->paid_date;

        /**
         * Were there any payment entries for this $id
         * If there are no entries then use the paid_date function for start_date
         * If there is a previous entry use the end_date of last entry for start_date
         */
        if(strpos($uri, 'subscriber') !== false) {
            $subscriber = Subscriber::find($id);
            $dependants[0] = $subscriber;
            $payment->dependants = count($dependants);
            $past_payments = $subscriber->payments;
            // dd($past_payments);
            
            if($past_payments->count() == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $last_payment = collect($past_payments)->last();
                $payment->start_date = $last_payment->end_date;
                // dd($payment->start_date);
            }    
        } elseif(strpos($uri, 'family') !== false) {
            $family = Family::find($id);
            $dependants = $family->subscribers;
            $payment->dependants = $dependants->count();
            $past_payments = $family->payments;

            if($past_payments->count() == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $last_payment = collect($past_payments)->last();
                $payment->start_date = $last_payment->end_date;
            }
        } elseif(strpos($uri, 'corporate') !== false) {
            $corporate = Corporate::find($id);
            $dependants = $corporate->subscribers;
            $payment->dependants = $dependants->count();
            $past_payments = $corporate->payments;

            if($past_payments->count() == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $last_payment = collect($past_payments)->last();
                $payment->start_date = $last_payment->end_date;
            }
        } elseif(strpos($uri, 'safari') !== false) {
            $safari = Safari::find($id);
            $dependants = $safari->subscribers;
            $payment->dependants = $dependants->count();
            $past_payments = $safari->payments;
            $beds = $safari->beds;

            if($past_payments->count() == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $last_payment = collect($past_payments)->last();
                $payment->start_date = $last_payment->end_date;
            }
        }

        // $payment->end_date = strtotime(Helper::rr_end_date($payment->amount, $uri, $past_payments, $dependants), strtotime($payment->start_date)); // calculate the end_date based on amount paid
        $payment->end_date = date('Y-m-d', strtotime($payment->start_date . Helper::rr_end_date($payment->amount, $uri, $maker, $last_payment, $dependants, $beds)));

        $payment->save();

        /**
         * Here we are checking to see where the payment originated
         * We then save it as either:
         * subscriber
         * family
         * corporate
         * safari
         */
        if (strpos($uri, 'subscriber') !== false) {
            $subscriber = Subscriber::find($id);
            $payment->subscriber()->sync($subscriber, false);
        } elseif (strpos($uri, 'family') !== false) {
            $family = Family::find($id);
            $payment->family()->sync($family, false);
        } elseif (strpos($uri, 'corporate') !== false) {
            $corporate = Corporate::find($id);
            $payment->corporate()->sync($corporate, false);
        } elseif (strpos($uri, 'safari') !== false) {
            $safari = Safari::find($id);
            $payment->safari()->sync($safari, false);
        }

        Session::flash('success', 'A new Payment was successfully saved!');

        // redirect
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $payment_id, $maker, $id)
    {   
        $uri = '';
        $dependants = [];
        $past_payments = [];
        $last_payment = [];
        $route = ''; // where to go after the edit has been made
        $beds = 0; // only for safari camps

        // validate data
        $this->validate($request, array(
            'amount' => 'required|digits_between:2,8',
            'paid_date' => 'required|date'
        ));

        // store in the database
        $payment = Payment::find($payment_id);

        $payment->amount = $request->amount;
        $payment->paid_date = $request->paid_date;

        /**
         * Were there any payment entries for this $id
         * If there are no entries then use the now() function for start_date
         * Because the system will look for the last entry we have to skip that
         * We ahve to look at the entry prior to that last
         * Then use the end_date of that entry for start_date
         */
        if($maker == 'subscriber') {
            $subscriber = Subscriber::find($id);
            $dependants[0] = $subscriber;
            $payment->dependants = count($dependants);
            $past_payments = $subscriber->payments;
            $last_payment = Helper::rr_last_payment(collect($past_payments), $payment_id); // find the payment before the payment_id, in terms of paid_date

            // dd($last_payment);
            if(count($last_payment) == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $payment->start_date = $last_payment[0]->end_date;
            }

            $route = redirect()->route('subscribers.show', $subscriber->id);

        } elseif($maker == 'family') {
            $family = Family::find($id);
            $dependants = $family->subscribers;
            $payment->dependants = $dependants->count();
            $past_payments = $family->payments;
            $last_payment = Helper::rr_last_payment(collect($past_payments), $payment_id); // find the payment before the payment_id, in terms of paid_date

            if(count($last_payment) == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $payment->start_date = $last_payment[0]->end_date;
            }

            $route = redirect()->route('family.show', $family->id);

        } elseif($maker == 'corporate') {
            $corporate = Corporate::find($id);
            $dependants = $corporate->subscribers;
            $payment->dependants = $dependants->count();
            $past_payments = $corporate->payments;
            $last_payment = Helper::rr_last_payment(collect($past_payments), $payment_id); // find the payment before the payment_id, in terms of paid_date

            if(count($last_payment) == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $payment->start_date = $last_payment[0]->end_date;
            }

            $route = redirect()->route('corporate.show', $corporate->id);
        
        } elseif($maker == 'safari') {
            $safari = Safari::find($id);
            $dependants = $safari->subscribers;
            $payment->dependants = $dependants->count();
            $past_payments = $safari->payments;
            $beds = $safari->beds;
            $last_payment = Helper::rr_last_payment(collect($past_payments), $payment_id); // find the payment before the payment_id, in terms of paid_date

            if(count($last_payment) == 0) {
                $payment->start_date = $payment->paid_date;
            } else {
                $payment->start_date = $last_payment[0]->end_date;
            }

            $route = redirect()->route('safari.show', $safari->id);
        }

        // $payment->end_date = strtotime(Helper::rr_end_date($payment->amount, $uri, $past_payments, $dependants), strtotime($payment->start_date)); // calculate the end_date based on amount paid
        $payment->end_date = date('Y-m-d', strtotime($payment->start_date . Helper::rr_end_date($payment->amount, $uri, $maker, $last_payment, $dependants, $beds)));

        $payment->save();

        Session::flash('success', 'A Payment was successfully updated!');

        // redirect
        return $route;
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $payment_id, $id)
    {
        $uri->uri = $request->url;

        $payment = Payment::find($payment_id);

        if(strpos($uri->uri, 'subscriber') !== false) {
            $payment->subscriber()->detach();
        } elseif(strpos($uri->uri, 'family') !== false) {
            $payment->family()->detach();
        } elseif(strpos($uri->uri, 'corporate') !== false) {
            $payment->corporate()->detach();
        } elseif(strpos($uri->uri, 'safari') !== false) {
            $payment->safari()->detach();
        }
        
        $payment->delete();
        
        Session::flash('success', 'The Payment was successfully deleted!');
        
        return redirect()->route('payment.index', $id);
    }
}
