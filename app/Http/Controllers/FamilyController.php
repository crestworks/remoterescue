<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

use App\Family;
use App\Subscriber;
use App\Group;
use App\Comment;
use App\Payment;

class FamilyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = Family::all();
        $subscribers = DB::table('family_subscriber')->get();
        $groups = DB::table('groups')->select('id', 'family_id')->where('family_id', '>', 0)->get();

        $count = $families->count();
        // $payment = collect($payments)->first();

        /**
         * Customize Families object
         * loop through the families object
         * count the number of dependants for each family
         * add new Key Value pair to families object
         * 'dependants' = 'number of dependants for the family'
         */
        
        for ($x = 0; $x < $count; $x++)
        {
            $families[$x]['dependants'] = $subscribers->where('family_id', '=', $families[$x]['id'])->count();
            
            $group = $groups->where('family_id', '=', $families[$x]['id']);
            $families[$x]['group_id'] = $group[$x]->id;
        }

        return view('family.index')
            ->withFamilies($families)
            ->withCount($count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // should have DB query here that eliminates subscribers that are already part of a Family Package
        
        // Family Package => package_id = 6
        $subscribers = Subscriber::all()->where('package_id', '=', '6');

        return view('family.create')
            ->withSubscribers($subscribers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'primary' => 'required'
        ));

        // store new family in the database
        $family = new Family;

        $family->name = $request->name;
        $family->primary = $request->primary; // must have a custom validation here to check only 1 is selected and that 1 is from the list of dependants

        $family->save();

        if (isset($request->subscribers)) {
            $family->subscribers()->sync($request->subscribers, true);
        } else {
            $family->subscribers()->sync(array(), true);
        }

        /**
         * Store family_id in Group DB
         * by maintaining the Groups table we can continue to use the Group IDs
         * that have already been published
         */

        $group = new Group;

        $group->family_id = $family->id;
        $group->corporate_id = null;
        $group->safari_id = null;

        $group->save();

        Session::flash('success', 'A Family group has been successfully created!');

        // redirect
        return redirect()->route('family.show', $family->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $family = Family::find($id);
        $group = DB::table('groups')->select('id', 'family_id')->get();
        $comments = $family->comments;
        $payments = $family->payments->sortByDesc('paid_date');
        
        $group_id = $group[0]->id;

        for ($x = 0; $x < $group->count(); $x++)
        {
            $group[$x]->family_id == $id ? $group_id = $group[$x]->id : '';
        }

        /**
         * Last amount paid
         * Most recent payment period
         * Days remaining
         * Last payment date
         */

        $payment = collect($payments)->first();

        return view('family.show')
            ->withFamily($family)
            ->withGroup_id($group_id)
            ->withComments($comments)
            ->withPayment($payment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::find($id);
        // Family Package => package_id = 6
        $subscribers = Subscriber::all()->where('package_id', '=', '6');
        $group = Group::all()->where('family_id', '=', $id);

        return view('family.edit')
            ->withFamily($family)
            ->withSubscribers($subscribers)
            ->withGroup($group);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'primary' => 'required'
        ));

        // store update family in the database
        $family = Family::find($id);

        $family->name = $request->name;
        $family->primary = $request->primary; // must have a custom validation here to check only 1 is selected and that 1 is from the list of dependants

        $family->save();

        if (isset($request->subscribers)) {
            $family->subscribers()->sync($request->subscribers, true);
        } else {
            $family->subscribers()->sync(array(), true);
        }

        Session::flash('success', 'The Family has been successfully updated!');

        // redirect
        return redirect()->route('family.show', $family->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    {
        //
    }
}
