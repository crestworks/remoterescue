<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscriber;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the results.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $request->search;

        // Basic search algorithm
        $results = Subscriber::where('first', 'LIKE', '%' . $query . '%')->orWhere('last', 'LIKE', '%' . $query . '%')->get();

        $count = $results->count();

        return view('search.index')->withResults($results)->withQuery($query)->withCount($count);
    }
}
