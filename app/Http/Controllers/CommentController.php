<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;

use App\Comment;
use App\Subscriber;
use App\Family;
use App\Corporate;
use App\Safari;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // This is the url that originated saving the comment
        $uri = url()->previous();

        // validate data
        $this->validate($request, array(
            'comment' => 'required|min:5|max:2000'
        ));

        // store in the database
        $comment = new Comment;

        $comment->comment = $request->comment;
        $comment->admin_id = Auth::user()->id;

        /**
         * Here we are checking to see where the comment originated
         * We then save it as either:
         * subscriber
         * family
         * corporate
         * safari
         */

        $comment->save();

        if(strpos($uri, 'subscriber') !== false) {
            $subscriber = Subscriber::find($id);
            $comment->subscriber()->sync($subscriber, false);
        } elseif(strpos($uri, 'family') !== false) {
            $family = Family::find($id);
            $comment->family()->sync($family, false);
        } elseif (strpos($uri, 'corporate') !== false) {
            $corporate = Corporate::find($id);
            $comment->corporate()->sync($corporate, false);
        } elseif (strpos($uri, 'safari') !== false) {
            $safari = Safari::find($id);
            $comment->safari()->sync($safari, false);
        }

        Session::flash('success', 'A new Comment was successfully saved!');

        // redirect
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

        // validate data
        $this->validate($request, array(
            'comment' => 'required|min:5|max:2000'
        ));

        // store in the database
        $comment->comment = $request->comment;
        // $comment->admin_id = Auth::user()->id;

        $comment->save();

        Session::flash('success', 'The Comment was successfully edited!');

        // redirect
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // This is the url that originated deleting the comment
        $uri = url()->previous();

        $comment = Comment::find($id);

        if(strpos($uri, 'subscriber') !== false) {
            $comment->subscriber()->detach();
        } elseif(strpos($uri, 'family') !== false) {
            $comment->family()->detach();
        } elseif (strpos($uri, 'corporate') !== false) {
            $comment->corporate()->detach();
        } elseif (strpos($uri, 'safari') !== false) {
            $comment->safari()->detach();
        }
        
        $comment->delete();
        
        Session::flash('success', 'The Comment was successfully deleted!');
        
        return redirect()->back();
    }
}
