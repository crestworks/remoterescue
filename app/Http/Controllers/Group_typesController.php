<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Group_type;

class Group_typesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group_types = Group_type::all();

        $active = $group_types->where('active', '=', '1')->count();
        $inactive = $group_types->where('active', '=', '0')->count();

        return view('group_types.index')->withGroupTypes($group_types)->withActive($active)->withInactive($inactive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required',
            'active' => 'required|boolean'
        ));

        // store in the database
        $group_type = new Group_type;

        $group_type->title = $request->title;
        $group_type->description = $request->description;
        $group_type->active = $request->active;

        $group_type->save();

        Session::flash('success', 'A new Remote Rescue Group Type was successfully saved!');

        // redirect
        return redirect()->route('group_types.show', $group_type->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group_type = Group_type::find($id);

        return view('group_types.show')->withGroup_type($group_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group_type = Group_type::find($id);

        return view('group_types.edit')->withGroup_type($group_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required',
            'active' => 'required|boolean'
        ));

        // Store data
        $group_type = Group_type::find($id);

        $group_type->title = $request->title;
        $group_type->description = $request->description;
        $group_type->active = $request->active;

        $group_type->save();

        Session::flash('success', 'A Remote Rescue Group Type was successfully updated!');

        // Redirect
        return redirect()->route('group_types.show', $group_type->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
