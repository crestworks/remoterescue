<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Session;
use Auth;

use App\Admin;
use App\Role;

class AdministratorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();
        $roles = Role::all();

        $count = $admins->count();
        $masters = $admins->where('role_id', '=', '1')->count();
        $editors = $admins->where('role_id', '=', '2')->count();
        $viewers = $admins->where('role_id', '=', '3')->count();

        return view('administrators.index')
            ->withAdmins($admins)
            ->withCount($count)
            ->withMasters($masters)
            ->withEditors($editors)
            ->withViewers($viewers)
            ->withRoles($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('administrators.index')
            ->withRoles($roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $roles = Role::all();
        
        // validate data
        $this->validate($request, array(
            'first' => 'required|string|max:255',
            'last' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',
            'mobile' => 'required|max:255',
            'job_title' => 'required|string|max:255',
            'role_id' => 'required|max:2',
            'username' => 'required|string|max:255|unique:admins',
            'password' => 'required|string|min:6',
        ));

        // store in the database
        $admin = new Admin;

        $admin->first = $request->first;
        $admin->last = $request->last;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->job_title = $request->job_title;
        $admin->role_id = $request->role_id;
        $admin->username = $request->username;
        $admin->password = bcrypt($request['password']);

        $admin->save();

        Session::flash('success', 'A new Admistrator was successfully added to the system!');

        // redirect
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Admin::find($id);
        $roles = Role::all();

        return view('administrators.show')
            ->withAdmin($admin)
            ->withRoles($roles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find($id);
        $roles = Role::all();

        return view('administrators.edit')
            ->withAdmin($admin)
            ->withRoles($roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::find($id);
        $roles = Role::all();

        // validate data
        $this->validate($request, array(
            'first' => 'required|string|max:255',
            'last' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',
            'mobile' => 'required|max:255',
            'job_title' => 'required|string|max:255',
            'role_id' => 'required|max:2',
            // 'username' => ['required', Rule::unique('admins')->ignore($admin->id)],
            // 'password' => 'required|string|min:6',
        ));

        // store in the database
        $admin->first = $request->first;
        $admin->last = $request->last;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->job_title = $request->job_title;
        $admin->role_id = $request->role_id;
        // $admin->username = $request->username;
        // $admin->password = bcrypt($request['password']);

        if(Auth::user()->id == $id) {
            $this->validate($request, array(
                'username' => ['required', Rule::unique('admins')->ignore($admin->id)],
            ));
            $admin->username = $request->username;
        }

        $admin->save();

        Session::flash('success', 'Administrator Profile has been successfully updated!');

        // redirect
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
