<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Comment_group;
use App\Group;

class Comment_groupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $group_id)
    {
        // validate data
        $this->validate($request, array(
            'comment' => 'required|min:5|max:2000'
        ));

        // store in the database
        $group = Group::find($group_id);

        $comment = new Comment_group;

        $comment->comment = $request->comment;
        $comment->group()->associate($group);

        $comment->save();

        Session::flash('success', 'A new Comment was successfully saved!');

        // redirect
        // return redirect()->route('groups.show', [$group->id]);
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $comment_id)
    {
        $comment = Comment_group::find($comment_id);

        // validate data
        $this->validate($request, array(
            'comment' => 'required|min:5|max:2000'
        ));

        // store in the database
        $comment->comment = $request->comment;

        $comment->save();

        Session::flash('success', 'The Comment was successfully edited!');

        // redirect
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
