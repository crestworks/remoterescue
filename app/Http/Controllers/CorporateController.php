<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

use App\Corporate;
use App\Subscriber;
use App\Group;
use App\Comment;
use App\Payment;

class CorporateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporates = Corporate::all();
        $subscribers = DB::table('corporate_subscriber')->get();
        $groups = DB::table('groups')->where('corporate_id', '>', 0)->get();
        
        $count = $corporates->count();
        // $payment = collect($payments)->first();

        /**
         * Customize corporates object
         * loop through the corporates object
         * count the number of dependants for each corporate
         * add new Key Value pair to families object
         * 'dependants' = 'number of dependants for the corporate'
         */

        for ($x = 0; $x < $count; $x++)
        {
            $corporates[$x]['dependants'] = $subscribers->where('corporate_id', '=', $corporates[$x]['id'])->count();
            
            $group = $groups->where('corporate_id', '=', $corporates[$x]['id']);
            $corporates[$x]['group_id'] = $group[$x]->id;
        }

        return view('corporate.index')
            ->withCorporates($corporates)
            ->withCount($count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // should have DB query here that eliminates subscribers that are already part of a Corporate Package
        // Corporate Package => package_id = 7
        $subscribers = Subscriber::all()->where('package_id', '=', '7');

        return view('corporate.create')
            ->withSubscribers($subscribers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'emergency_contact' => 'required',
            'emergency_phone' => 'required|max:255',
            'account_rep' => 'required',
            'account_rep_phone' => 'required|max:255',
            'account_rep_email' => 'required|email',
            'website' => 'nullable|url',
        ));

        // store new Corporate in the database
        $corporate = new Corporate;

        $corporate->name = $request->name;
        $corporate->address = $request->address;
        $corporate->emergency_contact = $request->emergency_contact;
        $corporate->emergency_phone = $request->emergency_phone;
        $corporate->account_rep = $request->account_rep;
        $corporate->account_rep_phone = $request->account_rep_phone;
        $corporate->account_rep_email = $request->account_rep_email;
        $corporate->website = $request->website;

        $corporate->save();

        if (isset($request->subscribers)) {
            $corporate->subscribers()->sync($request->subscribers, true);
        } else {
            $corporate->subscribers()->sync(array(), true);
        }

        /**
         * Store corporate_id in Group DB
         * by maintaining the Groups table we can continue to use the Group IDs
         * that have already been published
         */

        $group = new Group;

        $group->family_id = null;
        $group->corporate_id = $corporate->id;
        $group->safari_id = null;

        $group->save();

        Session::flash('success', 'A Corporate group has been successfully created!');

        // redirect
        return redirect()->route('corporate.show', $corporate->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $corporate = Corporate::find($id);
        $group = DB::table('groups')->select('id', 'corporate_id')->get();
        $comments = $corporate->comments;
        $payments = $corporate->payments->sortByDesc('paid_date');
        $emergency_contact = Subscriber::find($corporate->emergency_contact);
        $account_rep = Subscriber::find($corporate->account_rep);
        
        $group_id = $group[0]->id;

        for ($x = 0; $x < $group->count(); $x++)
        {
            $group[$x]->corporate_id == $id ? $group_id = $group[$x]->id : '';
        }

        /**
         * Last amount paid
         * Most recent payment period
         * Days remaining
         * Last payment date
         */

        $payment = collect($payments)->first();

        return view('corporate.show')
            ->withCorporate($corporate)
            ->withGroup_id($group_id)
            ->withComments($comments)
            ->withPayment($payment)
            ->withEmergency_contact($emergency_contact)
            ->withAccount_rep($account_rep);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $corporate = Corporate::find($id);
        // Corporate Package => package_id = 7
        $subscribers = Subscriber::all()->where('package_id', '=', '7');
        $group = Group::all()->where('corporate_id', '=', $id);

        return view('corporate.edit')->withCorporate($corporate)
            ->withSubscribers($subscribers)
            ->withGroup($group);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'emergency_contact' => 'required',
            'emergency_phone' => 'required|max:255',
            'account_rep' => 'required',
            'account_rep_phone' => 'required|max:255',
            'account_rep_email' => 'required|email',
            'website' => 'nullable|url',
        ));

        // store new Corporate in the database
        $corporate = Corporate::find($id);

        $corporate->name = $request->name;
        $corporate->address = $request->address;
        $corporate->emergency_contact = $request->emergency_contact;
        $corporate->emergency_phone = $request->emergency_phone;
        $corporate->account_rep = $request->account_rep;
        $corporate->account_rep_phone = $request->account_rep_phone;
        $corporate->account_rep_email = $request->account_rep_email;
        $corporate->website = $request->website;

        $corporate->save();

        if (isset($request->subscribers)) {
            $corporate->subscribers()->sync($request->subscribers, true);
        } else {
            $corporate->subscribers()->sync(array(), true);
        }

        Session::flash('success', 'A Corporate group has been successfully edited!');

        // redirect
        return redirect()->route('corporate.show', $corporate->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    {
        //
    }
}
