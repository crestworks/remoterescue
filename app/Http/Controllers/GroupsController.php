<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Group;
use App\Group_type;
use App\Subscriber;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group_types = Group_type::all()->where('active', '>', '0');

        return view('groups.create')->withGroup_types($group_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'group_name' => 'required|max:255',
            'group_type_id' => 'required|max:1'
        ));

        // store in the database
        $groups = new Group;

        $groups->group_name = $request->group_name;
        $groups->group_type_id = $request->group_type_id;

        $groups->save();

        Session::flash('success', 'A new Group was successfully saved!');

        // redirect
        return redirect()->route('groups.show', $groups->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::find($id);

        return view('groups.show')->withGroup($group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        $group_types = Group_type::all();
        $subscribers = Subscriber::all()->where('package_id', '=', '2');

        return view('groups.edit')->withGroup($group)->withGroup_types($group_types)->withSubscribers($subscribers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'group_name' => 'required|max:255',
            'group_type_id' => 'required|max:1'
        ));

        // store in the database
        $group = Group::find($id);

        $group->group_name = $request->group_name;
        $group->group_type_id = $request->group_type_id;

        $group->save();

        if (isset($request->subscribers)) {
            $group->subscribers()->sync($request->subscribers, true);
        } else {
            $group->subscribers()->sync(array(), true);
        }

        Session::flash('success', 'A Group has been successfully updated!');

        // redirect
        return redirect()->route('groups.show', $group->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);
        $group->subscribers()->detach();

        $group->delete();

        Session::flash('success', 'A Group has been successfully deleted!');

        // redirect
        return redirect()->route('group');        
    }
}
