<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

use App\Question;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        $active = $questions->where('active', '=', '1')->count();
        $inactive = $questions->where('active', '=', '0')->count();

        return view('questions.index')
            ->withQuestions($questions)
            ->withActive($active)
            ->withInactive($inactive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'question' => 'required|max:255',
            'expected' => 'required|boolean',
            'active' => 'required|boolean'
        ));

        // store in the database
        $question = new Question;

        $question->question = $request->question;
        $question->expected = $request->expected;
        $question->active = $request->active;

        $question->save();

        Session::flash('success', 'A new Medical Question was successfully saved!');

        // redirect
        return redirect()
        ->route('questions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);

        return view('questions.show')->withQuestion($question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);

        return view('questions.edit')->withQuestion($question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate data
        $this->validate($request, array(
            'question' => 'required|max:255',
            'expected' => 'required|boolean',
            'active' => 'required|boolean'
        ));

        // Store data
        $question = Question::find($id);

        $question->question = $request->question;
        $question->expected = $request->expected;
        $question->active = $request->active;

        $question->save();

        Session::flash('success', 'A Medical Question has successfully been updated!');

        // Redirect
        return redirect()
            ->route('questions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->subscribers()->detach();

        $question->delete();

        Session::flash('success', 'A Question has been successfully deleted!');

        // redirect
        return redirect()->route('questions.index');
    }
}
