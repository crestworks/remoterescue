<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
	public function __construct()
	{
         $this->middleware('guest:admin')->except('logout');
	}


    public function getIndex()
    {
        return view('welcome');
    }

	/*
	 * This is the Admin Login form
	*/
    public function showLoginForm()
    {
    	return view('auth.admin-login');
    }

    /*
	 * This is the login functionality
	*/
    public function login(Request $request)
    {
    	// Validate form data
    	$this->validate($request, [
    		'username' => 'required',
    		'password' => 'required|min:6'
    	]);

    	// Attempt to log the Admin in
    	if(Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->remember)) {
    		// If successful then redirect to intended location
            return redirect()->route('individual');
    	}

    	// If unsuccessful, then redirect back to login with form data
    	return redirect()->back()->withInput($request->only('username', 'remember'));
    }

    /*
     * This is the logout functionality
    */
    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('/');
    }

}
