<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

use App\Safari;
use App\Subscriber;
use App\Group;
use App\Comment;
use App\Payment;

class SafariController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $safaris = Safari::all();
        $subscribers = DB::table('safari_subscriber')->get();
        $groups = DB::table('groups')->where('safari_id', '>', 0)->get();
        
        $count = $safaris->count();
        // $payment = collect($payments)->first();

        /**
         * Customize safaris object
         * loop through the safaris object
         * count the number of dependants for each safari
         * add new Key Value pair to families object
         * 'dependants' = 'number of dependants for the safari'
         */

        for ($x = 0; $x < $count; $x++)
        {
            $safaris[$x]['dependants'] = $subscribers->where('safari_id', '=', $safaris[$x]['id'])->count();
            
            $group = $groups->where('safari_id', '=', $safaris[$x]['id']);
            $safaris[$x]['group_id'] = $group[$x]->id;
        }

        // dd($safaris);

        return view('safari.index')
            ->withSafaris($safaris)
            ->withCount($count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // should have DB query here that eliminates subscribers that are already part of a Corporate Package
        // Safari Package => package_id = 8
        $subscribers = Subscriber::all()->where('package_id', '=', '8');

        return view('safari.create')->withSubscribers($subscribers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'beds' => 'required|digits_between:1,4',
            'coordinates' => 'required|max:255',
            'location_description' => 'required|max:500',
            'emergency_contact' => 'required',
            'emergency_phone' => 'required|max:255',
            'account_rep' => 'required',
            'account_rep_phone' => 'required|max:255',
            'account_rep_email' => 'required|email',
            'website' => 'nullable|url',
            'nearest_ace' => 'required|max:255',
            'nearest_airstrip' => 'required|max:255'
        ));

        // store new Safari in the database
        $safari = new Safari;

        $safari->name = $request->name;
        $safari->beds = $request->beds;
        $safari->coordinates = $request->coordinates;
        $safari->location_description = $request->location_description;
        $safari->emergency_contact = $request->emergency_contact;
        $safari->emergency_phone = $request->emergency_phone;
        $safari->account_rep = $request->account_rep;
        $safari->account_rep_phone = $request->account_rep_phone;
        $safari->account_rep_email = $request->account_rep_email;
        $safari->website = $request->website;
        $safari->nearest_ace = $request->nearest_ace;
        $safari->nearest_airstrip = $request->nearest_airstrip;

        $safari->save();

        if (isset($request->subscribers)) {
            $safari->subscribers()->sync($request->subscribers, true);
        } else {
            $safari->subscribers()->sync(array(), true);
        }

        /**
         * Store corporate_id in Group DB
         * by maintaining the Groups table we can continue to use the Group IDs
         * that have already been published
         */

        $group = new Group;

        $group->family_id = null;
        $group->corporate_id = null;
        $group->safari_id = $safari->id;

        $group->save();

        Session::flash('success', 'A Safari group has been successfully created!');

        // redirect
        return redirect()->route('safari.show', $safari->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Safari  $corporate
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $safari = Safari::find($id);
        $group = DB::table('groups')->select('id', 'safari_id')->get();
        $comments = $safari->comments;
        $payments = $safari->payments->sortByDesc('paid_date');
        $emergency_contact = Subscriber::find($safari->emergency_contact);
        $account_rep = Subscriber::find($safari->account_rep);
        
        $group_id = $group[0]->id;

        for ($x = 0; $x < $group->count(); $x++)
        {
            $group[$x]->safari_id == $id ? $group_id = $group[$x]->id : '';
        }

        /**
         * Last amount paid
         * Most recent payment period
         * Days remaining
         * Last payment date
         */

        $payment = collect($payments)->first();

        return view('safari.show')
            ->withSafari($safari)
            ->withGroup_id($group_id)
            ->withComments($comments)
            ->withPayment($payment)
            ->withEmergency_contact($emergency_contact)
            ->withAccount_rep($account_rep);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Safari  $corporate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $safari = Safari::find($id);
        // Safari Package => package_id = 8
        $subscribers = Subscriber::all()->where('package_id', '=', '8');
        $group = Group::all()->where('safari_id', '=', $id);

        return view('safari.edit')
            ->withSafari($safari)
            ->withSubscribers($subscribers)
            ->withGroup($group);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Safari  $safari
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'beds' => 'required|digits_between:1,4',
            'coordinates' => 'required|max:255',
            'location_description' => 'required|max:500',
            'emergency_contact' => 'required',
            'emergency_phone' => 'required|max:255',
            'account_rep' => 'required',
            'account_rep_phone' => 'required|max:255',
            'account_rep_email' => 'required|email',
            'website' => 'nullable|url',
            'nearest_ace' => 'required|max:255',
            'nearest_airstrip' => 'required|max:255'
        ));

        // store new Safari in the database
        $safari = Safari::find($id);

        $safari->name = $request->name;
        $safari->beds = $request->beds;
        $safari->coordinates = $request->coordinates;
        $safari->location_description = $request->location_description;
        $safari->emergency_contact = $request->emergency_contact;
        $safari->emergency_phone = $request->emergency_phone;
        $safari->account_rep = $request->account_rep;
        $safari->account_rep_phone = $request->account_rep_phone;
        $safari->account_rep_email = $request->account_rep_email;
        $safari->website = $request->website;
        $safari->nearest_ace = $request->nearest_ace;
        $safari->nearest_airstrip = $request->nearest_airstrip;

        $safari->save();

        if (isset($request->subscribers)) {
            $safari->subscribers()->sync($request->subscribers, true);
        } else {
            $safari->subscribers()->sync(array(), true);
        }

        Session::flash('success', 'A Safari group has been successfully edited!');

        // redirect
        return redirect()->route('safari.show', $safari->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    {
        //
    }
}
