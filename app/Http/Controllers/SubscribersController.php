<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;

use App\Subscriber;
use App\Family;
use App\Corporate;
use App\Safari;
use App\Package;
use App\Question;
use App\Admin;
use App\Comment;
use App\Payment;

class SubscribersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packages = Package::all()->where('active', '>', '0');
        $questions = Question::all();

        return view('subscribers.create')
            ->withPackages($packages)
            ->withQuestions($questions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // validate data
        $this->validate($request, array(
            'last' => 'required|max:255',
            'first' => 'required|max:255',
            'weight' => 'required|max:200|numeric',
            'dob' => 'required|date',
            'email' => 'required|email',
            'mobile' => 'required', //add validation for mobile, clear anything extra, must start with a +, and the rest must be numbers
            'idNumber' => 'required|unique:subscribers,idNumber', //add validation to clear all spaces
            // 'accountNumber' => 'required',
            'package_id' => 'required'
        ));


        // store in the database
        $subscriber = new Subscriber;

        $subscriber->last = $request->last;
        $subscriber->first = $request->first;
        $subscriber->weight = $request->weight;
        $subscriber->dob = $request->dob;
        $subscriber->email = $request->email;
        $subscriber->mobile = $request->mobile;
        $subscriber->idNumber = $request->idNumber;
        $subscriber->package_id = $request->package_id;
        $subscriber->vetting = -1;

        // ==========

        $subscriber->save();

        // Creating array for many to many table

        $answers = collect($request->answers);

        $inputs = $answers->map(function ($items, $keys) {
            return ['answer' => $items];
        })->all();

        // ==========

        $subscriber->questions()->sync($inputs, false);

        Session::flash('success', 'A new Subscriber was successfully saved!');

        // redirect
        return redirect()->route('subscribers.show', $subscriber->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $uri = url()->previous(); // if the request comes from family, corporate, or safari this will capture it

        $subscriber = Subscriber::find($id);
        $comments = $subscriber->comments;
        $payments = $subscriber->payments->sortByDesc('paid_date');
        $group = collect();

        if($subscriber->package_id == 6) {
            if(DB::table('family_subscriber')->where('subscriber_id', '=', $id)->get()->count()) {
                $family = DB::table('family_subscriber')->where('subscriber_id', '=', $id)->get();
                $group = Family::find($family[0]->family_id);
            }
        }

        if($subscriber->package_id == 7) {
            if(DB::table('corporate_subscriber')->where('subscriber_id', '=', $id)->get()->count()) {
                $corporate = DB::table('corporate_subscriber')->where('subscriber_id', '=', $id)->get();
                $group = Corporate::find($corporate[0]->corporate_id);
            }
        }

        if($subscriber->package_id == 8) {
            if(DB::table('safari_subscriber')->where('subscriber_id', '=', $id)->get()->count()) {
                $safari = DB::table('safari_subscriber')->where('subscriber_id', '=', $id)->get();
                $group = Safari::find($safari[0]->safari_id);
            }
        }

        $questions = Question::all();
        $payment = collect($payments)->first();

        return view('subscribers.show')
            ->withSubscriber($subscriber)
            ->withComments($comments)
            ->withPayment($payment)
            ->withQuestions($questions)
            ->withGroup($group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscriber = Subscriber::find($id);
        // $packages = Package::all();
        $packages = Package::all()->where('active', '>', '0');
        $questions = Question::all();

        // Answered Questions
        $answered = collect([]);
        foreach ($subscriber->questions as $question) {
            $answered->push($question->id);
        }

        // Unanswered Questions
        $un_answered = $questions->diffKeys($answered);

        return view('subscribers.edit')->withSubscriber($subscriber)->withPackages($packages)->withQuestions($questions)->withUn_answered($un_answered);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {      
        $subscriber = Subscriber::find($id);

        // validate data
        $this->validate($request, array(
            'last' => 'required|max:255',
            'first' => 'required|max:255',
            'weight' => 'required|max:200|numeric',
            'dob' => 'required|date',
            'email' => 'required|email',
            'mobile' => 'required', //add validation for mobile, clear anything extra, must start with a +, and the rest must be numbers
            'idNumber' => ['required', Rule::unique('subscribers')->ignore($subscriber->id)], //add validation to clear all spaces
            // 'accountNumber' => ['required', Rule::unique('subscribers')->ignore($subscriber->id)],
            'package_id' => 'required'
        ));

        // store in the database
        $subscriber->last = $request->last;
        $subscriber->first = $request->first;
        $subscriber->weight = $request->weight;
        $subscriber->dob = $request->dob;
        $subscriber->email = $request->email;
        $subscriber->mobile = $request->mobile;
        $subscriber->idNumber = $request->idNumber;
        $subscriber->package_id = $request->package_id;
        $subscriber->vetting = $request->vetting;

        $subscriber->save();

        // Creating array for many to many table

        $answers = collect($request->answers);

        $inputs = $answers->map(function ($items, $keys) {
            return ['answer' => $items];
        })->all();

        // ==========

        if (isset($inputs)) {
            $subscriber->questions()->sync($inputs, true);
        } else {
            $subscriber->questions()->sync(array(), true);
        }

        Session::flash('success', 'Subscriber Profile has been successfully updated!');

        // redirect
        return redirect()->route('subscribers.show', $subscriber->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscriber = Subscriber::find($id);
        $subscriber->groups()->detach();
        $subscriber->questions()->detach();

        $subscriber->delete();

        Session::flash('success', 'A Subscriber has been successfully deleted!');

        // redirect
        return redirect()->route('individual');
    }
}
