<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

use App\Subscriber;
use App\Group;
use App\Payment;
use App\Group_type;
use App\Family;

class DashboardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndividual()
    {
        $subscribers = Subscriber::all()->where('package_id', '=', '1');
        $payments = Payment::all();

        $count = $subscribers->count();
        $processing = $subscribers->where('vetting', '<', '0')->count();
        $fail = $subscribers->where('vetting', '=', '0')->count();

        return view('dashboards.individual')
            ->withSubscribers($subscribers)
            ->withCount($count)
            ->withProcessing($processing)
            ->withFail($fail);
    }

    public function getRegion()
    {
        $subscribers = Subscriber::all()->where('package_id', '=', '3');

        $count = $subscribers->count();
        $processing = $subscribers->where('vetting', '<', '0')->count();
        $fail = $subscribers->where('vetting', '=', '0')->count();

        return view('dashboards.region')
            ->withSubscribers($subscribers)
            ->withCount($count)
            ->withProcessing($processing)
            ->withFail($fail);
    }

    public function getRegional()
    {
        $subscribers = Subscriber::all()->where('package_id', '=', '4');

        $count = $subscribers->count();
        $processing = $subscribers->where('vetting', '<', '0')->count();
        $fail = $subscribers->where('vetting', '=', '0')->count();

        return view('dashboards.regional')
            ->withSubscribers($subscribers)
            ->withCount($count)
            ->withProcessing($processing)
            ->withFail($fail);
    }

    public function getDaily()
    {
        $subscribers = Subscriber::all()->where('package_id', '=', '5');

        $count = $subscribers->count();
        $processing = $subscribers->where('vetting', '<', '0')->count();
        $fail = $subscribers->where('vetting', '=', '0')->count();

        return view('dashboards.daily')
            ->withSubscribers($subscribers)
            ->withCount($count)
            ->withProcessing($processing)
            ->withFail($fail);
    }
}
