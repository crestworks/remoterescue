<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Package;

class PackagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();

        $active = $packages->where('active', '=', '1')->count();
        $inactive = $packages->where('active', '=', '0')->count();

        return view('packages.index')->withPackages($packages)->withActive($active)->withInactive($inactive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required',
            'active' => 'required|boolean'
        ));

        // store in the database
        $package = new Package;

        $package->title = $request->title;
        $package->description = $request->description;
        $package->active = $request->active;

        $package->save();

        Session::flash('success', 'A new Remote Rescue Package was successfully saved!');

        // redirect
        return redirect()->route('packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);

        return view('packages.show')->withPackage($package);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::find($id);

        return view('packages.edit')->withPackage($package);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required',
            'active' => 'required|boolean'
        ));

        // Store data
        $package = Package::find($id);

        $package->title = $request->title;
        $package->description = $request->description;
        $package->active = $request->active;

        $package->save();

        Session::flash('success', 'A Remote Rescue Package was successfully updated!');

        // Redirect
        return redirect()->route('packages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete data
        $package = Package::find($id);
        
        $package->delete();
        
        Session::flash('success', 'The Package was successfully deleted!');
        
        return redirect()->route('packages.index');
    }
}
