<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('daily', 'DashboardsController@getDaily')->name('daily');

Route::get('regional', 'DashboardsController@getRegional')->name('regional');

Route::get('region', 'DashboardsController@getRegion')->name('region');

// Route::get('group', 'DashboardsController@getGroup')->name('group');

// Route::get('families', 'DashboardsController@getFamily')->name('families');

// Route::get('corporate', 'DashboardsController@getCorporate')->name('corporate');

// Route::get('safari', 'DashboardsController@getSafari')->name('safari');

Route::get('individual', 'DashboardsController@getIndividual')->name('individual');

Route::get('/', 'Auth\AdminLoginController@getIndex');

// Search Subscribers
Route::post('search', 'SearchController@index')->name('search');

// Administrator Management
Route::resource('administrators', 'AdministratorsController');

// Subscriber Management
Route::resource('subscribers', 'SubscribersController');

// Packages Management
Route::resource('packages', 'PackagesController');

// Group Type Management
// Route::resource('group_types', 'Group_typesController');

// Question Bank Management
Route::resource('questions', 'QuestionsController');

// Family Management
Route::resource('family', 'FamilyController');

// Corporate Management
Route::resource('corporate', 'CorporateController');

// Corporate Management
Route::resource('safari', 'SafariController');

// Groups Management
// Route::resource('groups', 'GroupsController');

// Comments
Route::post('comment/{id}', ['uses' => 'CommentController@store', 'as' => 'comment.store']);
Route::put('comment/{id}', ['uses' => 'CommentController@update', 'as' => 'comment.update']);
Route::delete('comment/{id}', ['uses' => 'CommentController@destroy', 'as' => 'comment.destroy']);
// ========

// Group Comments
// Route::post('comment_groups/{group_id}', ['uses' => 'Comment_groupsController@store', 'as' => 'comment_group.store']);
// Route::put('comment_groups/{comment_id}', ['uses' => 'Comment_groupsController@update', 'as' => 'comment_group.update']);
// ========

// Payments
Route::get('payment/{id}', ['uses' => 'PaymentController@index', 'as' => 'payment.index']); // the id here is the id of the subscriber, family, corporate, safari
Route::post('payment/{id}', ['uses' => 'PaymentController@store', 'as' => 'payment.store']); // the id here is the id of the subscriber, family, corporate, safari
Route::put('payment/{payment_id}/{maker}/{id}', ['uses' => 'PaymentController@update', 'as' => 'payment.update']); // the 'maker' is the whoever is making the payment
Route::delete('payment/{payment_id}/{maker}/{id}', ['uses' => 'PaymentController@destroy', 'as' => 'payment.destroy']);
// ========

// Admin Login
Route::prefix('admin')->group(function() {
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	// Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

	// Password reset routes
	Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
	Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});
// =========


