# Remote Rescue

## About This Program

The Remote Rescue database is an administrative platform to collect and manage the Subscribers to the ACE Air & Ambulance Remote Rescue service.
This database is specifically create to meet our company needs but there are many subscription services out there that can benifit from this project, of course amendments to the code will need to be made to fit your company.
We have decided to make this Project open source so that the community can benefit from this, as a starting point, and we can benefit from the communities input. Thus create a more rebust Subscription Management service.

## This Project was developed using the following resources:

#### - Laravel (PHP Framework) 5.4
#### - Bootstrap (CSS Framework) 4.1.1
#### - jQuery (Javascript Framework) 3.3.1

## Server Side Deployment:

#### - Lunix
#### - Ubuntu 18.04
#### - Nginx
