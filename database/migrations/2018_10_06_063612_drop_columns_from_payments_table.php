<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsFromPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('subscriber_id');
            $table->dropColumn('package_id');
            $table->dropColumn('start_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('subscriber_id')->after('id')->nullable()->unsigned();
            $table->integer('package_id')->after('id')->nullable()->unsigned();
            $table->integer('start_date')->after('id')->nullable()->unsigned();
        });
    }
}
