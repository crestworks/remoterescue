<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->integer('safari_id')->after('admin_id')->nullable()->unsigned();
            $table->integer('corporate_id')->after('admin_id')->nullable()->unsigned();
            $table->integer('family_id')->after('admin_id')->nullable()->unsigned();
            $table->integer('subscriber_id')->after('admin_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('admin_id');
            $table->dropColumn('family_id');
            $table->dropColumn('corporate_id');
            $table->dropColumn('safari_id');
        });
    }
}
