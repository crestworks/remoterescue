<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safaris', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('beds');
            $table->string('coordinates');
            $table->text('location_description');
            $table->integer('emergency_contact')->unsigned();
            $table->string('emergency_phone');
            $table->integer('account_rep')->unsigned();
            $table->string('account_rep_phone');
            $table->string('account_rep_email');
            $table->string('website')->nullable();
            $table->string('nearest_ace');
            $table->string('nearest_airstrip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safaris');
    }
}
