<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('name'); original
            $table->string('first');
            $table->string('last');
            $table->string('weight');
            $table->date('dob');
            // $table->string('email')->unique(); original
            $table->string('email');
            $table->string('mobile');
            $table->string('idNumber')->unique();
            $table->integer('package_id');
            $table->boolean('vetting')->nullable();
            // $table->string('username')->unique(); add when users have access to profile
            // $table->string('password'); original
            // $table->rememberToken(); original
            $table->timestamps();

            // $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}


